drop database if exists db_school_payment_smp;
drop database if exists db_school_payment_smk;
drop database if exists db_school_payment_yayasan;
create database db_school_payment_smp;
create database db_school_payment_smk;
create database db_school_payment_yayasan;

use db_school_payment_smp;

create table programmes
(
    id int not null auto_increment,
    name varchar(30) not null,
    primary key(id),
    unique(name)
);

create table grades
(
    id int not null auto_increment,
    name varchar(10) not null,
    primary key(id)
);

create table students
(
    id int not null auto_increment,
    student_id varchar(10) not null,
    name varchar(100) not null,
    programme_id int not null,
    grade_id int not null,
    status varchar(10) not null,
    primary key(id),
    foreign key(programme_id) references programmes(id) on delete cascade,
    foreign key(grade_id) references grades(id) on delete cascade
);

create table billing_types
(
    id int not null auto_increment,
    name varchar(50) not null,
    day_interval int not null,
    primary key(id),
    unique(name)
);

create table billing_links
(
    billing_type_id int not null,
    programme_id int not null,
    amount int not null,
    foreign key(billing_type_id) references billing_types(id) on delete cascade,
    foreign key(programme_id) references programmes(id) on delete cascade
);

create table transaction_categories
(
    id int not null auto_increment,
    name varchar(50) not null,
    primary key(id),
    unique(name)
);

create table transactions
(
    id varchar(15) not null,
    t_time datetime not null,
    amount int not null,
    payer varchar(50) not null,
    is_out bit not null,
    details varchar(100),
    category_id int,
    primary key(id)
);

create table student_payments
(
    transaction_id varchar(15) not null,
    student_id int not null,
    billing_type_id int not null,
    year int,
    semester varchar(20),
    month varchar(15),
    foreign key(transaction_id) references transactions(id) on delete cascade,
    foreign key(student_id) references students(id) on delete cascade,
    foreign key(billing_type_id) references billing_types(id) on delete cascade
);

create table savings
(
    student_id int not null,
    t_time datetime not null,
    amount int not null,
    is_out bit not null,
    foreign key(student_id) references students(id) on delete cascade
);

use db_school_payment_smk;

create table programmes
(
    id int not null auto_increment,
    name varchar(30) not null,
    primary key(id),
    unique(name)
);

create table grades
(
    id int not null auto_increment,
    name varchar(10) not null,
    primary key(id)
);

create table students
(
    id int not null auto_increment,
    student_id varchar(10) not null,
    name varchar(100) not null,
    programme_id int not null,
    grade_id int not null,
    status varchar(10) not null,
    primary key(id),
    foreign key(programme_id) references programmes(id) on delete cascade,
    foreign key(grade_id) references grades(id) on delete cascade
);

create table billing_types
(
    id int not null auto_increment,
    name varchar(50) not null,
    day_interval int not null,
    primary key(id),
    unique(name)
);

create table billing_links
(
    billing_type_id int not null,
    programme_id int not null,
    amount int not null,
    foreign key(billing_type_id) references billing_types(id) on delete cascade,
    foreign key(programme_id) references programmes(id) on delete cascade
);

create table transaction_categories
(
    id int not null auto_increment,
    name varchar(50) not null,
    primary key(id),
    unique(name)
);

create table transactions
(
    id varchar(15) not null,
    t_time datetime not null,
    amount int not null,
    payer varchar(50) not null,
    is_out bit not null,
    details varchar(100),
    category_id int,
    primary key(id)
);

create table student_payments
(
    transaction_id varchar(15) not null,
    student_id int not null,
    billing_type_id int not null,
    year int,
    semester varchar(20),
    month varchar(15),
    foreign key(transaction_id) references transactions(id) on delete cascade,
    foreign key(student_id) references students(id) on delete cascade,
    foreign key(billing_type_id) references billing_types(id) on delete cascade
);

create table savings
(
    student_id int not null,
    t_time datetime not null,
    amount int not null,
    is_out bit not null,
    foreign key(student_id) references students(id) on delete cascade
);

use db_school_payment_yayasan;

create table programmes
(
    id int not null auto_increment,
    name varchar(30) not null,
    primary key(id),
    unique(name)
);

create table grades
(
    id int not null auto_increment,
    name varchar(10) not null,
    primary key(id)
);

create table students
(
    id int not null auto_increment,
    student_id varchar(10) not null,
    name varchar(100) not null,
    programme_id int not null,
    grade_id int not null,
    status varchar(10) not null,
    primary key(id),
    foreign key(programme_id) references programmes(id) on delete cascade,
    foreign key(grade_id) references grades(id) on delete cascade
);

create table billing_types
(
    id int not null auto_increment,
    name varchar(50) not null,
    day_interval int not null,
    primary key(id),
    unique(name)
);

create table billing_links
(
    billing_type_id int not null,
    programme_id int not null,
    amount int not null,
    foreign key(billing_type_id) references billing_types(id) on delete cascade,
    foreign key(programme_id) references programmes(id) on delete cascade
);

create table transaction_categories
(
    id int not null auto_increment,
    name varchar(50) not null,
    primary key(id),
    unique(name)
);

create table transactions
(
    id varchar(15) not null,
    t_time datetime not null,
    amount int not null,
    payer varchar(50) not null,
    is_out bit not null,
    details varchar(100),
    category_id int,
    primary key(id)
);

create table student_payments
(
    transaction_id varchar(15) not null,
    student_id int not null,
    billing_type_id int not null,
    year int,
    semester varchar(20),
    month varchar(15),
    foreign key(transaction_id) references transactions(id) on delete cascade,
    foreign key(student_id) references students(id) on delete cascade,
    foreign key(billing_type_id) references billing_types(id) on delete cascade
);

create table savings
(
    student_id int not null,
    t_time datetime not null,
    amount int not null,
    is_out bit not null,
    foreign key(student_id) references students(id) on delete cascade
);

drop user 'pusat'@'localhost';
create user 'pusat'@'localhost' identified by 'pusatoke';
grant usage,select,insert,update,delete on db_school_payment_smp.* to 'pusat'@'localhost';
grant usage,select,insert,update,delete on db_school_payment_smk.* to 'pusat'@'localhost';
grant usage,select,insert,update,delete on db_school_payment_yayasan.* to 'pusat'@'localhost';