﻿using Microsoft.Reporting.WinForms;
using MySql.Data.MySqlClient;
using school_payment.Commons;
using school_payment.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace school_payment.Views
{
    /// <summary>
    /// Interaction logic for ManageTransactionsPage.xaml
    /// </summary>
    public partial class ManageTransactionsPage : Page
    {
        // View models.
        class TransactionViewModel
        {
            public Transaction Model { get; private set; }

            public string Id
            {
                get
                {
                    return Model.Id;
                }
            }

            public string Payer
            {
                get
                {
                    return Model.Payer;
                }
            }

            public int Amount
            {
                get
                {
                    return Model.Amount;
                }
            }

            public DateTime TTime
            {
                get
                {
                    return Model.TTime;
                }
            }

            public string TypeString
            {
                get
                {
                    return Model.IsOutTransaction ? "OUT" : "IN";
                }
            }

            public string Details
            {
                get
                {
                    return Model.Details;
                }
            }


            public TransactionViewModel(Transaction model)
            {
                Model = model;
            }

            public override bool Equals(object obj)
            {
                TransactionViewModel comp = obj as TransactionViewModel;

                if (comp != null)
                {
                    return Model.Equals(comp.Model);
                }

                return false;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }

        // Page definition.
        MainWindow m_mainwindow;
        bool m_canGoBack;
        ObservableCollection<TransactionViewModel> m_transactions;
        ObservableCollection<TransactionCategory> m_transactionCategories;


        public ManageTransactionsPage(MainWindow mainwindow)
        {
            InitializeComponent();

            m_transactions = new ObservableCollection<TransactionViewModel>();
            lstTransactions.ItemsSource = m_transactions;

            m_transactionCategories = new ObservableCollection<TransactionCategory>();
            cmbFilterCategory.ItemsSource = m_transactionCategories;
            cmbCategory_gridTransaction.ItemsSource = m_transactionCategories;

            m_mainwindow = mainwindow;
            m_mainwindow.BackRequested += M_mainwindow_BackRequested;

            Init();
        }

        private void M_mainwindow_BackRequested(MainWindow sender, Page content)
        {
            if (content == this && m_canGoBack)
            {
                m_mainwindow.BackRequested -= M_mainwindow_BackRequested;

                m_transactions.Clear();
                cmbType_gridTransaction.Items.Clear();

                m_mainwindow.GoBack();
            }
        }

        async void Init()
        {
            m_mainwindow.PlayLoadingAnimation();
            dtFilterStart.IsEnabled = false;
            dtFilterEnd.IsEnabled = false;

            try
            {
                cmbFilterType.Items.Add("IN");
                cmbFilterType.Items.Add("OUT");

                cmbType_gridTransaction.Items.Add("IN");
                cmbType_gridTransaction.Items.Add("OUT");

                dtFilterStart.SelectedDate = DateTime.Now.Date;
                dtFilterEnd.SelectedDate = DateTime.Now.AddDays(1).Date;

                await LoadTransactionsAsync();

                await LoadTransactionCategoriesAsync();
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to initialize.\nDetails: " + ex.Message);
            }
            finally
            {
                m_mainwindow.StopLoadingAnimation();
                m_canGoBack = true;
                dtFilterEnd.IsEnabled = true;
                dtFilterStart.IsEnabled = true;
            }
        }

        async Task LoadTransactionsAsync()
        {
            m_transactions.Clear();

            if (dtFilterStart.SelectedDate.HasValue && dtFilterEnd.SelectedDate.HasValue && dtFilterStart.SelectedDate.Value > dtFilterEnd.SelectedDate.Value)
            {
                throw new Exception("Start date is greater than the end date");
            }

            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbconn;

                    string typeFilter = cmbFilterType.SelectedItem as string;
                    cmd.CommandText = "SELECT * FROM transactions WHERE (t_time BETWEEN @filterStart AND @filterEnd) ";

                    if (typeFilter == "IN")
                    {
                        cmd.CommandText += "AND is_out = 0 ";
                    }
                    else if (typeFilter == "OUT")
                    {
                        cmd.CommandText += "AND is_out = 1 ";
                    }

                    TransactionCategory selcategory = cmbFilterCategory.SelectedItem as TransactionCategory;

                    if (selcategory != null)
                    {
                        cmd.CommandText += "AND category_id = @categoryId ";
                        cmd.Parameters.AddWithValue("categoryId", selcategory.Id);
                    }

                    cmd.CommandText += "ORDER BY t_time DESC";

                    cmd.Parameters.AddWithValue("filterStart", dtFilterStart.SelectedDate.HasValue ? dtFilterStart.SelectedDate.Value : DateTime.MinValue);
                    cmd.Parameters.AddWithValue("filterEnd", dtFilterEnd.SelectedDate.HasValue ? dtFilterEnd.SelectedDate.Value.Date.AddDays(1) : DateTime.MaxValue);

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            Transaction t = new Transaction();
                            t.Id = reader.GetString(0);
                            t.TTime = reader.GetDateTime(1);
                            t.Amount = reader.GetInt32(2);
                            t.Payer = reader.GetString(3);
                            t.IsOutTransaction = reader.GetBoolean(4);
                            t.Details = reader.GetString(5);
                            t.Category = await reader.IsDBNullAsync(6) ? null : await Loader.GetTransactionCategoryByIdAsync(reader.GetInt32(6));

                            // Adding name to details.
                            //t.Details += " - " + t.Payer;

                            m_transactions.Add(new TransactionViewModel(t));
                        }
                    }
                }
            }
        }

        async Task LoadTransactionCategoriesAsync()
        {
            m_transactionCategories.Clear();

            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbconn;
                    cmd.CommandText = "SELECT * FROM transaction_categories";

                    using (MySqlDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            TransactionCategory tc = new TransactionCategory();
                            tc.Id = reader.GetInt32(0);
                            tc.Name = reader.GetString(1);

                            m_transactionCategories.Add(tc);
                        }
                    }
                }
            }
        }

        private void dtFilterStart_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            dtFilterEnd_SelectedDateChanged(null, null);
        }

        private async void dtFilterEnd_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!m_canGoBack)
            {
                return; // Sometimes this fires twice. :(
            }

            m_mainwindow.PlayLoadingAnimation();
            m_canGoBack = false;

            try
            {
                await LoadTransactionsAsync();
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to load transactions.\nDetails: " + ex.Message);
            }
            finally
            {
                m_mainwindow.StopLoadingAnimation();
                m_canGoBack = true;
            }
        }

        private async void cmbFilterType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!m_canGoBack)
            {
                return; // Sometimes this fires twice. :(
            }

            m_mainwindow.PlayLoadingAnimation();
            m_canGoBack = false;

            try
            {
                await LoadTransactionsAsync();
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to load transactions.\nDetails: " + ex.Message);
            }
            finally
            {
                m_mainwindow.StopLoadingAnimation();
                m_canGoBack = true;
            }
        }

        private async void cmbFilterCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!m_canGoBack)
            {
                return; // Sometimes this fires twice. :(
            }

            m_mainwindow.PlayLoadingAnimation();
            m_canGoBack = false;

            try
            {
                await LoadTransactionsAsync();
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to load transactions.\nDetails: " + ex.Message);
            }
            finally
            {
                m_mainwindow.StopLoadingAnimation();
                m_canGoBack = true;
            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            gridTransaction.Visibility = Visibility.Visible;
            m_canGoBack = false;
        }

        private async void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            TransactionViewModel selitem = lstTransactions.SelectedItem as TransactionViewModel;

            if (selitem != null)
            {
                if (!await m_mainwindow.ShowPromptAsync("Confirmation", "Are you sure want to delete selected item?"))
                {
                    return;
                }

                btnDelete.IsEnabled = false;
                m_mainwindow.PlayLoadingAnimation();

                try
                {
                    using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
                    {
                        await dbconn.OpenAsync();

                        using (MySqlCommand cmd = new MySqlCommand())
                        {
                            cmd.Connection = dbconn;
                            cmd.CommandText = "DELETE FROM transactions WHERE id = @id";
                            cmd.Parameters.AddWithValue("id", selitem.Id);
                            await cmd.ExecuteNonQueryAsync();
                        }
                    }

                    m_transactions.Remove(selitem);
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to delete selected item.\nDetails: " + ex.Message);
                }
                finally
                {
                    btnDelete.IsEnabled = true;
                    m_mainwindow.StopLoadingAnimation();
                }
            }
        }

        private async void btnSave_gridTransaction_Click(object sender, RoutedEventArgs e)
        {
            btnSave_gridTransaction.IsEnabled = false;
            btnCancel_gridTransaction.IsEnabled = false;

            try
            {
                // Validating.
                if (string.IsNullOrEmpty(txtPayer_gridTransaction.Text))
                {
                    throw new Exception("Payer name is empty");
                }

                int amount;

                if (!int.TryParse(txtAmount_gridTransaction.Text, out amount))
                {
                    throw new Exception("Amount is empty or invalid");
                }

                if (string.IsNullOrEmpty(txtDetails_gridTransaction.Text))
                {
                    throw new Exception("Details is empty");
                }

                if (cmbType_gridTransaction.SelectedItem == null)
                {
                    throw new Exception("Type not selected");
                }

                // Adding to transactions table.
                using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
                {
                    await dbconn.OpenAsync();

                    DateTime now = DateTime.Now;

                    using (MySqlCommand cmd = new MySqlCommand())
                    {
                        cmd.Connection = dbconn;
                        cmd.CommandText = "INSERT INTO transactions VALUES(@id, @time, @amount, @payer, @isOut, @details, @categoryId)";
                        cmd.Parameters.AddWithValue("id", await Loader.GenerateTransactionIdAsync(dbconn, null, now));
                        cmd.Parameters.AddWithValue("time", now);
                        cmd.Parameters.AddWithValue("amount", amount);
                        cmd.Parameters.AddWithValue("payer", txtPayer_gridTransaction.Text);
                        cmd.Parameters.AddWithValue("isOut", cmbType_gridTransaction.SelectedItem as string == "OUT" ? 1 : 0);
                        cmd.Parameters.AddWithValue("details", txtDetails_gridTransaction.Text);

                        TransactionCategory selcategory = cmbCategory_gridTransaction.SelectedItem as TransactionCategory;

                        if (selcategory == null)
                        {
                            cmd.Parameters.AddWithValue("categoryId", null);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("categoryId", selcategory.Id);
                        }

                        await cmd.ExecuteNonQueryAsync();
                    }
                }

                // Refreshing the list.
                await LoadTransactionsAsync();

                // Done.
                btnCancel_gridTransaction_Click(null, null);
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to add new transaction.\nDetails: " + ex.Message);
            }
            finally
            {
                btnSave_gridTransaction.IsEnabled = true;
                btnCancel_gridTransaction.IsEnabled = true;
            }
        }

        private void btnCancel_gridTransaction_Click(object sender, RoutedEventArgs e)
        {
            m_canGoBack = true;
            gridTransaction.Visibility = Visibility.Hidden;

            txtPayer_gridTransaction.Clear();
            txtAmount_gridTransaction.Clear();
            txtDetails_gridTransaction.Clear();
            cmbType_gridTransaction.SelectedItem = null;
            cmbCategory_gridTransaction.SelectedItem = null;
        }

        private void btnPrintTransaction_Click(object sender, RoutedEventArgs e)
        {
            if (m_transactions.Count == 0)
            {
                m_mainwindow.ShowMessage("Error", "No transaction to be printed!");
                return;
            }

            List<TransactionPrint> tps = new List<TransactionPrint>();
            int outTotal = 0, inTotal = 0;

            // Earlier -> later ordering.
            for (int i = m_transactions.Count - 1; i >= 0; --i)
            {
                Transaction t = m_transactions[i].Model;

                if (t.IsOutTransaction)
                {
                    outTotal += t.Amount;
                }
                else
                {
                    inTotal += t.Amount;
                }

                tps.Add(new TransactionPrint(t));
            }

            ReportDataSource rds = new ReportDataSource("TransactionPrintDataSet", tps);

            List<ReportParameter> reportParameters = new List<ReportParameter>();
            reportParameters.Add(new ReportParameter("OutTotalString", outTotal.ToString("C")));
            reportParameters.Add(new ReportParameter("InTotalString", inTotal.ToString("C")));
            reportParameters.Add(new ReportParameter("SubTotalString", (inTotal - outTotal).ToString("C")));
            reportParameters.Add(new ReportParameter("DateString", DateTime.Now.ToString("d MMMM yyyy")));

            PrintWindow p = new PrintWindow("Resources\\Templates\\TransactionReport.rdlc", rds, reportParameters);
            p.Show();
        }
    }
}
