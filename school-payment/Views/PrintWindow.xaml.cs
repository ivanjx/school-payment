﻿using MahApps.Metro.Controls;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace school_payment.Views
{
    /// <summary>
    /// Interaction logic for PrintWindow.xaml
    /// </summary>
    public partial class PrintWindow : MetroWindow
    {
        public PrintWindow(string templatePath, ReportDataSource dsource = null, List<ReportParameter> reportParams = null)
        {
            InitializeComponent();

            rv.LocalReport.ReportPath = templatePath;

            if (dsource != null)
            {
                rv.LocalReport.DataSources.Add(dsource);
            }

            if (reportParams != null)
            {
                rv.LocalReport.SetParameters(reportParams);
            }

            rv.RefreshReport();
        }

        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            rv.Dispose();
        }
    }
}
