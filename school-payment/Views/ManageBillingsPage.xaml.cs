﻿using MySql.Data.MySqlClient;
using school_payment.Commons;
using school_payment.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace school_payment.Views
{
    /// <summary>
    /// Interaction logic for ManageBillingsPage.xaml
    /// </summary>
    public partial class ManageBillingsPage : Page
    {
        // View models.
        class BillingTypeViewModel
        {
            public BillingType Model { get; private set; }

            public string Name
            {
                get
                {
                    return Model.Name;
                }
            }

            public string PeriodName
            {
                get
                {
                    return Loader.GetPeriodName(Model.DayInterval);
                }
            }


            public BillingTypeViewModel(BillingType model)
            {
                Model = model;
            }

            public override bool Equals(object obj)
            {
                BillingTypeViewModel comp = obj as BillingTypeViewModel;

                if (comp != null)
                {
                    return Model.Equals(comp.Model);
                }

                return false;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }

        class BillingLinkViewModel : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            public BillingLink Model { get; private set; }

            bool m_isChecked;

            public bool IsChecked
            {
                get
                {
                    return m_isChecked;
                }
                set
                {
                    m_isChecked = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsChecked"));
                }
            }

            public string BillingTypeName
            {
                get
                {
                    return Model.BillingType.Name;
                }
            }

            public string BillingPeriodName
            {
                get
                {
                    return Loader.GetPeriodName(Model.BillingType.DayInterval);
                }
            }

            public int Amount
            {
                get
                {
                    return Model.Amount;
                }
                set
                {
                    Model.Amount = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Amount"));
                }
            }


            public BillingLinkViewModel(BillingLink model)
            {
                Model = model;
                IsChecked = false;
            }

            public override bool Equals(object obj)
            {
                BillingLinkViewModel comp = obj as BillingLinkViewModel;

                if (comp != null)
                {
                    return Model.Equals(comp.Model);
                }

                return false;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }

        // Page definition.
        MainWindow m_mainwindow;
        bool m_canGoBack;
        ObservableCollection<Programme> m_programmes;
        ObservableCollection<BillingLinkViewModel> m_billingLinks;
        ObservableCollection<BillingTypeViewModel> m_billingTypes;
        BillingTypeViewModel m_billingTypeBeingEdited;


        public ManageBillingsPage(MainWindow mainwindow)
        {
            InitializeComponent();

            m_programmes = new ObservableCollection<Programme>();
            cmbProgramme_Links.ItemsSource = m_programmes;

            m_billingTypes = new ObservableCollection<BillingTypeViewModel>();
            lstBillingTypes.ItemsSource = m_billingTypes;

            m_billingLinks = new ObservableCollection<BillingLinkViewModel>();
            lstBilling_Links.ItemsSource = m_billingLinks;

            m_mainwindow = mainwindow;
            m_mainwindow.BackRequested += M_mainwindow_BackRequested;

            Init();
        }

        private void M_mainwindow_BackRequested(MainWindow sender, Page content)
        {
            if (content == this && m_canGoBack)
            {
                m_mainwindow.BackRequested -= M_mainwindow_BackRequested;

                m_billingTypes.Clear();
                m_billingLinks.Clear();
                m_programmes.Clear();
                cmbPeriod_gridBillingType.Items.Clear();

                m_mainwindow.GoBack();
            }
        }

        async void Init()
        {
            m_mainwindow.PlayLoadingAnimation();

            try
            {
                await LoadBillingTypesAsync();
                await LoadProgrammesAsync();

                cmbPeriod_gridBillingType.Items.Add(StaticData.PERIOD_STRING_ONCE);
                cmbPeriod_gridBillingType.Items.Add(StaticData.PERIOD_STRING_ANNUALLY);
                cmbPeriod_gridBillingType.Items.Add(StaticData.PERIOD_STRING_SEMESTER);
                cmbPeriod_gridBillingType.Items.Add(StaticData.PERIOD_STRING_MONTHLY);
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to initialize.\nDetails: " + ex.Message);
            }
            finally
            {
                m_mainwindow.StopLoadingAnimation();
                m_canGoBack = true;
            }
        }

        async Task LoadBillingTypesAsync()
        {
            m_billingTypes.Clear();

            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbconn;
                    cmd.CommandText = "SELECT * FROM billing_types ORDER BY name ASC";

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            BillingType bt = new BillingType();
                            bt.Id = reader.GetInt32(0);
                            bt.Name = reader.GetString(1);
                            bt.DayInterval = reader.GetInt32(2);

                            m_billingTypes.Add(new BillingTypeViewModel(bt));
                        }
                    }
                }
            }
        }

        async Task LoadProgrammesAsync()
        {
            m_programmes.Clear();

            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbconn;
                    cmd.CommandText = "SELECT * FROM programmes ORDER BY name ASC";

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            Programme p = new Programme();
                            p.Id = reader.GetInt32(0);
                            p.Name = reader.GetString(1);

                            m_programmes.Add(p);
                        }
                    }
                }
            }
        }

        async Task LoadBillingLinksAsync(Programme programme)
        {
            m_billingLinks.Clear();

            // Loading all billing types.
            for (int i = 0; i < m_billingTypes.Count; ++i)
            {
                BillingLink link = new BillingLink();
                link.BillingType = m_billingTypes[i].Model;
                link.Programme = programme;

                m_billingLinks.Add(new BillingLinkViewModel(link));
            }

            // Applying links.
            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbconn;
                    cmd.CommandText = "SELECT billing_type_id, amount FROM billing_links WHERE programme_id = @programmeId";
                    cmd.Parameters.AddWithValue("programmeId", programme.Id);

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            BillingType bt = await Loader.GetBillingTypeByIdAsync(reader.GetInt32(0));
                            int amount = reader.GetInt32(1);

                            for (int i = 0; i < m_billingLinks.Count; ++i)
                            {
                                if (m_billingLinks[i].Model.BillingType.Equals(bt))
                                {
                                    m_billingLinks[i].Amount = amount;
                                    m_billingLinks[i].IsChecked = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void btnAddBillingType_Click(object sender, RoutedEventArgs e)
        {
            gridBillingType.Visibility = Visibility.Visible;
            m_canGoBack = false;
        }

        private async void btnDeleteBillingType_Click(object sender, RoutedEventArgs e)
        {
            BillingTypeViewModel selitem = lstBillingTypes.SelectedItem as BillingTypeViewModel;

            if (selitem != null)
            {
                if (!await m_mainwindow.ShowPromptAsync("Confirmation", "Are you sure want to delete selected item?"))
                {
                    return;
                }

                btnDeleteBillingType.IsEnabled = false;
                m_mainwindow.PlayLoadingAnimation();

                try
                {
                    using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
                    {
                        await dbconn.OpenAsync();

                        using (MySqlCommand cmd = new MySqlCommand())
                        {
                            cmd.Connection = dbconn;
                            cmd.CommandText = "DELETE FROM billing_types WHERE id = @id";
                            cmd.Parameters.AddWithValue("id", selitem.Model.Id);
                            await cmd.ExecuteNonQueryAsync();
                        }
                    }

                    m_billingTypes.Remove(selitem);

                    if (cmbProgramme_Links.SelectedItem != null)
                    {
                        await LoadBillingLinksAsync(cmbProgramme_Links.SelectedItem as Programme);
                    }
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to delete selected item.\nDetails: " + ex.Message);
                }
                finally
                {
                    btnDeleteBillingType.IsEnabled = true;
                    m_mainwindow.StopLoadingAnimation();
                }
            }
        }

        private void btnEditBillingType_Click(object sender, RoutedEventArgs e)
        {
            m_billingTypeBeingEdited = lstBillingTypes.SelectedItem as BillingTypeViewModel;

            if (m_billingTypeBeingEdited != null)
            {
                txtName_gridBillingType.Text = m_billingTypeBeingEdited.Name;
                cmbPeriod_gridBillingType.SelectedItem = Loader.GetPeriodName(m_billingTypeBeingEdited.Model.DayInterval);

                gridBillingType.Visibility = Visibility.Visible;
                m_canGoBack = false;
            }
        }

        private async void btnSave_gridBillingType_Click(object sender, RoutedEventArgs e)
        {
            btnSave_gridBillingType.IsEnabled = false;
            btnCancel_gridBillingType.IsEnabled = false;
            m_mainwindow.PlayLoadingAnimation();

            try
            {
                if (string.IsNullOrEmpty(txtName_gridBillingType.Text))
                {
                    throw new Exception("Type name is empty");
                }

                if (cmbPeriod_gridBillingType.SelectedItem == null)
                {
                    throw new Exception("Period not selected");
                }

                using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
                {
                    await dbconn.OpenAsync();

                    using (MySqlCommand cmd = new MySqlCommand())
                    {
                        cmd.Connection = dbconn;

                        if (m_billingTypeBeingEdited == null)
                        {
                            cmd.CommandText = "INSERT INTO billing_types(name, day_interval) VALUES(@name, @dayInt)";
                        }
                        else
                        {
                            cmd.CommandText = "UPDATE billing_types SET name = @name, day_interval = @dayInt WHERE id = @id";
                            cmd.Parameters.AddWithValue("id", m_billingTypeBeingEdited.Model.Id);
                        }

                        cmd.Parameters.AddWithValue("name", txtName_gridBillingType.Text);
                        cmd.Parameters.AddWithValue("dayInt", Loader.GetDayIntervalPeriod(cmbPeriod_gridBillingType.SelectedItem as string));
                        await cmd.ExecuteNonQueryAsync();
                    }
                }

                await LoadBillingTypesAsync();

                if (cmbProgramme_Links.SelectedItem != null)
                {
                    await LoadBillingLinksAsync(cmbProgramme_Links.SelectedItem as Programme);
                }

                btnCancel_gridBillingType_Click(null, null);
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to save changes.\nDetails: " + ex.Message);
            }
            finally
            {
                btnSave_gridBillingType.IsEnabled = true;
                btnCancel_gridBillingType.IsEnabled = true;
                m_mainwindow.StopLoadingAnimation();
            }
        }

        private void btnCancel_gridBillingType_Click(object sender, RoutedEventArgs e)
        {
            m_canGoBack = true;
            m_billingTypeBeingEdited = null;
            gridBillingType.Visibility = Visibility.Hidden;

            txtName_gridBillingType.Clear();
            cmbPeriod_gridBillingType.SelectedItem = null;
        }

        private async void cmbProgramme_Links_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbProgramme_Links.SelectedItem == null)
            {
                return;
            }
            
            cmbProgramme_Links.IsEnabled = false;
            m_mainwindow.PlayLoadingAnimation();

            try
            {
                Programme selprogramme = cmbProgramme_Links.SelectedItem as Programme;
                await LoadBillingLinksAsync(selprogramme);
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to load links.\nDetails: " + ex.Message);
            }
            finally
            {
                cmbProgramme_Links.IsEnabled = true;
                m_mainwindow.StopLoadingAnimation();
            }
        }

        private async void btnSave_Link_Click(object sender, RoutedEventArgs e)
        {
            cmbProgramme_Links.IsEnabled = false;
            lstBilling_Links.IsEnabled = false;
            btnSave_gridBillingType.IsEnabled = false;
            btnCancel_gridBillingType.IsEnabled = false;
            m_mainwindow.PlayLoadingAnimation();

            try
            {
                if (cmbProgramme_Links.SelectedItem == null)
                {
                    throw new Exception("Programme not selected");
                }
                
                Programme selprogramme = cmbProgramme_Links.SelectedItem as Programme;

                using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
                {
                    await dbconn.OpenAsync();

                    using (MySqlTransaction transaction = await dbconn.BeginTransactionAsync())
                    {
                        // Cleaning existing data.
                        using (MySqlCommand cmd = new MySqlCommand())
                        {
                            cmd.Connection = dbconn;
                            cmd.Transaction = transaction;
                            cmd.CommandText = "DELETE FROM billing_links WHERE programme_id = @programmeId";
                            cmd.Parameters.AddWithValue("programmeId", selprogramme.Id);
                            await cmd.ExecuteNonQueryAsync();
                        }

                        // Applying new data.
                        for (int i = 0; i < m_billingLinks.Count; ++i)
                        {
                            if (m_billingLinks[i].IsChecked)
                            {
                                using (MySqlCommand cmd = new MySqlCommand())
                                {
                                    cmd.Connection = dbconn;
                                    cmd.Transaction = transaction;
                                    cmd.CommandText = "INSERT INTO billing_links VALUES(@billingTypeId, @programmeId, @amount)";
                                    cmd.Parameters.AddWithValue("billingTypeId", m_billingLinks[i].Model.BillingType.Id);
                                    cmd.Parameters.AddWithValue("programmeId", selprogramme.Id);
                                    cmd.Parameters.AddWithValue("amount", m_billingLinks[i].Amount);
                                    await cmd.ExecuteNonQueryAsync();
                                }
                            }
                        }

                        // Done.
                        transaction.Commit();
                    }
                }

                // Reloading data.
                await LoadBillingLinksAsync(selprogramme);
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to save changes.\nDetails: " + ex.Message);
            }
            finally
            {
                cmbProgramme_Links.IsEnabled = true;
                lstBilling_Links.IsEnabled = true;
                btnSave_gridBillingType.IsEnabled = true;
                btnCancel_gridBillingType.IsEnabled = true;
                m_mainwindow.StopLoadingAnimation();
            }
        }

        private async void btnCancel_Link_Click(object sender, RoutedEventArgs e)
        {
            if (cmbProgramme_Links.SelectedItem == null)
            {
                return;
            }

            btnCancel_gridBillingType.IsEnabled = false;
            btnSave_gridBillingType.IsEnabled = false;
            m_mainwindow.PlayLoadingAnimation();

            try
            {
                await LoadBillingLinksAsync(cmbProgramme_Links.SelectedItem as Programme);
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to load links.\nDetails: " + ex.Message);
            }
            finally
            {
                btnCancel_gridBillingType.IsEnabled = true;
                btnSave_gridBillingType.IsEnabled = true;
                m_mainwindow.StopLoadingAnimation();
            }
        }
    }
}
