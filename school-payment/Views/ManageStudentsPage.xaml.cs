﻿using Microsoft.Win32;
using MySql.Data.MySqlClient;
using school_payment.Commons;
using school_payment.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace school_payment.Views
{
    /// <summary>
    /// Interaction logic for ManageStudentsPage.xaml
    /// </summary>
    public partial class ManageStudentsPage : Page
    {
        // View models.
        class StudentViewModel : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            bool m_isChecked;

            public Student Model
            {
                get;
                private set;
            }

            public bool IsChecked
            {
                get
                {
                    return m_isChecked;
                }
                set
                {
                    m_isChecked = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsChecked"));
                }
            }


            public StudentViewModel(Student model)
            {
                Model = model;
            }

            public override bool Equals(object obj)
            {
                StudentViewModel comp = obj as StudentViewModel;

                if (comp != null)
                {
                    return Model.Equals(comp.Model);
                }

                return false;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }

        // Page definition.
        MainWindow m_mainwindow;
        bool m_canGoBack;
        ObservableCollection<Grade> m_grades;
        ObservableCollection<Programme> m_programmes;
        ObservableCollection<StudentViewModel> m_students;
        StudentViewModel[] m_studentsBeingEdited;


        public ManageStudentsPage(MainWindow mainWindow)
        {
            InitializeComponent();

            m_students = new ObservableCollection<StudentViewModel>();
            lstStudents.ItemsSource = m_students;

            m_grades = new ObservableCollection<Grade>();
            cmbGrades.ItemsSource = m_grades;
            cmbGrade_gridStudent.ItemsSource = m_grades;

            m_programmes = new ObservableCollection<Programme>();
            cmbProgrammes.ItemsSource = m_programmes;
            cmbProgramme_gridStudent.ItemsSource = m_programmes;

            m_mainwindow = mainWindow;
            m_mainwindow.BackRequested += M_mainwindow_BackRequested;

            Init();
        }

        private void M_mainwindow_BackRequested(MainWindow sender, Page content)
        {
            if (content == this && m_canGoBack)
            {
                m_mainwindow.BackRequested -= M_mainwindow_BackRequested;

                m_students.Clear();
                m_grades.Clear();
                m_programmes.Clear();
                cmbStatus.Items.Clear();
                cmbStatus_gridStudent.Items.Clear();

                m_mainwindow.GoBack();
            }
        }

        async void Init()
        {
            m_mainwindow.PlayLoadingAnimation();
            m_canGoBack = false;

            try
            {
                await LoadGradesAsync();
                await LoadProgrammesAsync();
                await LoadStudentsAsync();

                cmbStatus_gridStudent.Items.Add("ACTIVE");
                cmbStatus_gridStudent.Items.Add("NOT ACTIVE");

                cmbStatus.Items.Add("ACTIVE");
                cmbStatus.Items.Add("NOT ACTIVE");
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to initialize.\nDetails: " + ex.Message);
            }
            finally
            {
                m_mainwindow.StopLoadingAnimation();
                m_canGoBack = true;
            }
        }

        async Task LoadStudentsAsync()
        {
            m_students.Clear();

            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbconn;
                    cmd.CommandText = "SELECT * FROM students ";

                    int filterCount = 0;

                    if (cmbStatus.SelectedItem != null)
                    {
                        cmd.CommandText += "WHERE status = @status";
                        cmd.Parameters.AddWithValue("status", cmbStatus.SelectedItem as string);
                        ++filterCount;
                    }

                    if (cmbGrades.SelectedItem != null)
                    {
                        if (filterCount == 0)
                        {
                            cmd.CommandText += "WHERE ";
                        }
                        else
                        {
                            cmd.CommandText += " AND ";
                        }

                        Grade selgrade = cmbGrades.SelectedItem as Grade;
                        cmd.CommandText += "grade_id = @gradeId";
                        cmd.Parameters.AddWithValue("gradeId", selgrade.Id);

                        ++filterCount;
                    }

                    if (cmbProgrammes.SelectedItem != null)
                    {
                        if (filterCount == 0)
                        {
                            cmd.CommandText += "WHERE ";
                        }
                        else
                        {
                            cmd.CommandText += " AND ";
                        }

                        Programme selprogramme = cmbProgrammes.SelectedItem as Programme;
                        cmd.CommandText += "programme_id = @programmeId";
                        cmd.Parameters.AddWithValue("programmeId", selprogramme.Id);
                    }

                    cmd.CommandText += " ORDER BY name ASC";

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            Student s = new Student();
                            s.Id = reader.GetInt32(0);
                            s.StudentId = reader.GetString(1);
                            s.Name = reader.GetString(2);
                            s.Programme = await Loader.GetProgrammeByIdAsync(reader.GetInt32(3));
                            s.Grade = await Loader.GetGradeByIdAsync(reader.GetInt32(4));
                            s.Status = reader.GetString(5);

                            m_students.Add(new StudentViewModel(s));
                        }
                    }
                }
            }
        }

        async Task LoadGradesAsync()
        {
            m_grades.Clear();

            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbconn;
                    cmd.CommandText = "SELECT * FROM grades";

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            Grade g = new Grade();
                            g.Id = reader.GetInt32(0);
                            g.Name = reader.GetString(1);

                            m_grades.Add(g);
                        }
                    }
                }
            }
        }

        async Task LoadProgrammesAsync()
        {
            m_programmes.Clear();

            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbconn;
                    cmd.CommandText = "SELECT * FROM programmes ORDER BY name ASC";

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            Programme p = new Programme();
                            p.Id = reader.GetInt32(0);
                            p.Name = reader.GetString(1);

                            m_programmes.Add(p);
                        }
                    }
                }
            }
        }

        private async void cmbGrades_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cmbGrades.IsEnabled = false;
            cmbProgrammes.IsEnabled = false;
            cmbStatus.IsEnabled = false;

            try
            {
                await LoadStudentsAsync();
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to load students.\nDetails: " + ex.Message);
            }
            finally
            {
                cmbGrades.IsEnabled = true;
                cmbProgrammes.IsEnabled = true;
                cmbStatus.IsEnabled = true;
            }
        }

        private void cmbProgrammes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cmbGrades_SelectionChanged(null, null);
        }

        private void cmbStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cmbGrades_SelectionChanged(null, null);
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            gridStudent.Visibility = Visibility.Visible;
            m_canGoBack = false;
        }

        private async void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            StudentViewModel[] selitems = (from student in m_students
                                           where student.IsChecked
                                           select student).ToArray();

            if (selitems.Length > 0)
            {
                if (!await m_mainwindow.ShowPromptAsync("Confirmation", "Are you sure want to delete selected item?"))
                {
                    return;
                }

                btnDelete.IsEnabled = false;
                m_mainwindow.PlayLoadingAnimation();

                try
                {
                    using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
                    {
                        await dbconn.OpenAsync();

                        for (int i = 0; i < selitems.Length; ++i)
                        {
                            using (MySqlCommand cmd = new MySqlCommand())
                            {
                                cmd.Connection = dbconn;
                                cmd.CommandText = "DELETE FROM students WHERE id = @id";
                                cmd.Parameters.AddWithValue("id", selitems[i].Model.Id);
                                await cmd.ExecuteNonQueryAsync();
                            }

                            m_students.Remove(selitems[i]);
                        }
                    }
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to delete selected item.\nDetails: " + ex.Message);
                }
                finally
                {
                    btnDelete.IsEnabled = true;
                    m_mainwindow.StopLoadingAnimation();
                }
            }
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            m_studentsBeingEdited = (from student in m_students
                                     where student.IsChecked
                                     select student).ToArray();

            if (m_studentsBeingEdited.Length == 1)
            {
                txtId_gridStudent.IsEnabled = true;
                txtName_gridStudent.IsEnabled = true;
                cmbProgramme_gridStudent.IsEnabled = true;
                cmbStatus_gridStudent.IsEnabled = true;

                txtId_gridStudent.Text = m_studentsBeingEdited[0].Model.StudentId;
                txtName_gridStudent.Text = m_studentsBeingEdited[0].Model.Name;
                cmbGrade_gridStudent.SelectedItem = m_studentsBeingEdited[0].Model.Grade;
                cmbProgramme_gridStudent.SelectedItem = m_studentsBeingEdited[0].Model.Programme;
                cmbStatus_gridStudent.SelectedItem = m_studentsBeingEdited[0].Model.Status;

                // Showing grid.
                gridStudent.Visibility = Visibility.Visible;
                m_canGoBack = false;
            }
            else if (m_studentsBeingEdited.Length > 1)
            {
                // Can only edit grade.
                txtId_gridStudent.IsEnabled = false;
                txtName_gridStudent.IsEnabled = false;
                cmbProgramme_gridStudent.IsEnabled = false;
                cmbStatus_gridStudent.IsEnabled = false;

                cmbGrades.SelectedItem = null;

                // Showing grid.
                gridStudent.Visibility = Visibility.Visible;
                m_canGoBack = false;
            }
            else // if 0.
            {
                m_studentsBeingEdited = null;
            }
        }

        private async void btnSave_gridStudent_Click(object sender, RoutedEventArgs e)
        {
            btnSave_gridStudent.IsEnabled = false;
            btnCancel_gridStudent.IsEnabled = false;
            m_mainwindow.PlayLoadingAnimation();

            try
            {
                using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
                {
                    await dbconn.OpenAsync();

                    if (m_studentsBeingEdited != null && m_studentsBeingEdited.Length > 1)
                    {
                        // Mass edit grades.
                        // Validating.
                        if (cmbGrade_gridStudent.SelectedItem == null)
                        {
                            throw new Exception("Grade not selected");
                        }

                        // Saving.
                        for (int i = 0; i < m_studentsBeingEdited.Length; ++i)
                        {
                            using (MySqlCommand cmd = new MySqlCommand())
                            {
                                cmd.Connection = dbconn;
                                cmd.CommandText = "UPDATE students SET grade_id = @gradeId WHERE id = @id";
                                cmd.Parameters.AddWithValue("id", m_studentsBeingEdited[i].Model.Id);
                                cmd.Parameters.AddWithValue("gradeId", (cmbGrade_gridStudent.SelectedItem as Grade).Id);
                                await cmd.ExecuteNonQueryAsync();
                            }
                        }
                    }
                    else
                    {
                        // Add new or edit one.
                        // Validating.
                        if (string.IsNullOrEmpty(txtId_gridStudent.Text))
                        {
                            throw new Exception("Student id is empty");
                        }

                        if (string.IsNullOrEmpty(txtName_gridStudent.Text))
                        {
                            throw new Exception("Name is empty");
                        }

                        if (cmbGrade_gridStudent.SelectedItem == null)
                        {
                            throw new Exception("Grade not selected");
                        }

                        if (cmbProgramme_gridStudent.SelectedItem == null)
                        {
                            throw new Exception("Programme not selected");
                        }

                        if (cmbStatus_gridStudent.SelectedItem == null)
                        {
                            throw new Exception("Status not selected");
                        }

                        // Saving.
                        using (MySqlCommand cmd = new MySqlCommand())
                        {
                            cmd.Connection = dbconn;

                            if (m_studentsBeingEdited == null)
                            {
                                cmd.CommandText = "INSERT INTO students(student_id, name, grade_id, programme_id, status) ";
                                cmd.CommandText += "VALUES(@studentId, @name, @gradeId, @programmeId, @status)";
                            }
                            else if (m_studentsBeingEdited.Length == 1)
                            {
                                cmd.CommandText = "UPDATE students SET student_id = @studentId, name = @name, programme_id = @programmeId, grade_id = @gradeId, status = @status ";
                                cmd.CommandText += "WHERE id = @id";
                                cmd.Parameters.AddWithValue("id", m_studentsBeingEdited[0].Model.Id);
                            }

                            cmd.Parameters.AddWithValue("studentId", txtId_gridStudent.Text);
                            cmd.Parameters.AddWithValue("name", txtName_gridStudent.Text);
                            cmd.Parameters.AddWithValue("gradeId", (cmbGrade_gridStudent.SelectedItem as Grade).Id);
                            cmd.Parameters.AddWithValue("programmeId", (cmbProgramme_gridStudent.SelectedItem as Programme).Id);
                            cmd.Parameters.AddWithValue("status", cmbStatus_gridStudent.SelectedItem as string);
                            await cmd.ExecuteNonQueryAsync();
                        }
                    }
                }

                await LoadStudentsAsync();
                btnCancel_gridStudent_Click(null, null);
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to save changes.\nDetails: " + ex.Message);
            }
            finally
            {
                btnSave_gridStudent.IsEnabled = true;
                btnCancel_gridStudent.IsEnabled = true;
                m_mainwindow.StopLoadingAnimation();
            }
        }

        private void btnCancel_gridStudent_Click(object sender, RoutedEventArgs e)
        {
            m_canGoBack = true;
            m_studentsBeingEdited = null;
            gridStudent.Visibility = Visibility.Hidden;

            txtId_gridStudent.Clear();
            txtName_gridStudent.Clear();
            cmbGrade_gridStudent.SelectedItem = null;
            cmbProgramme_gridStudent.SelectedItem = null;
            cmbStatus_gridStudent.SelectedItem = null;
        }

        private async void btnUploadForm_gridStudent_Click(object sender, RoutedEventArgs e)
        {
            btnUploadForm_gridStudent.IsEnabled = false;

            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Title = "Choose file...";
                ofd.Filter = "CSV File | *.csv";
                bool? result = ofd.ShowDialog();

                if (result.HasValue && result.Value)
                {
                    // Opening db conn.
                    using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
                    {
                        await dbconn.OpenAsync();

                        using (MySqlTransaction transaction = await dbconn.BeginTransactionAsync())
                        {
                            // Opening file.
                            using (StreamReader reader = new StreamReader(File.Open(ofd.FileName, FileMode.Open)))
                            {
                                // Reading line by line.
                                string line;
                                while ((line = await reader.ReadLineAsync()) != null)
                                {
                                    if (line[0] == '#')
                                    {
                                        continue; // This must be a comment.
                                    }

                                    string[] lineSplit = line.Split(new char[] { ',', ';' });

                                    // Saving to database.
                                    using (MySqlCommand cmd = new MySqlCommand())
                                    {
                                        Grade grade = await Loader.GetGradeByNameAsync(lineSplit[2]);
                                        Programme programme = await Loader.GetProgrammeByNameAsync(lineSplit[3]);

                                        cmd.Connection = dbconn;
                                        cmd.Transaction = transaction;
                                        cmd.CommandText = "INSERT INTO students(student_id, name, grade_id, programme_id, status) ";
                                        cmd.CommandText += "VALUES(@studentId, @name, @gradeId, @programmeId, @status)";
                                        cmd.Parameters.AddWithValue("studentId", lineSplit[0]);
                                        cmd.Parameters.AddWithValue("name", lineSplit[1]);
                                        cmd.Parameters.AddWithValue("gradeId", grade.Id);
                                        cmd.Parameters.AddWithValue("programmeId", programme.Id);
                                        cmd.Parameters.AddWithValue("status", lineSplit[4]);
                                        await cmd.ExecuteNonQueryAsync();
                                    }
                                }
                            }

                            // Commit.
                            transaction.Commit();
                        }
                    }

                    // Reloading list.
                    await LoadStudentsAsync();
                }
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to import data.\nDetails: " + ex.Message);
            }
            finally
            {
                btnUploadForm_gridStudent.IsEnabled = true;
            }
        }

        private async void btnDownloadForm_gridStudent_Click(object sender, RoutedEventArgs e)
        {
            btnDownloadForm_gridStudent.IsEnabled = false;

            try
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Title = "Save to...";
                sfd.Filter = "CSV File | *.csv";
                sfd.DefaultExt = ".csv";
                bool? result = sfd.ShowDialog();

                if (result.HasValue && result.Value)
                {
                    // Writing sample file.
                    using (StreamWriter writer = new StreamWriter(File.Open(sfd.FileName, FileMode.Create)))
                    {
                        await writer.WriteAsync("# NIS, # Nama, # Kelas, # Jurusan, # Status");
                    }
                }
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to create sample form.\nDetails: " + ex.Message);
            }
            finally
            {
                btnDownloadForm_gridStudent.IsEnabled = true;
            }
        }
    }
}
