﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace school_payment.Views
{
    /// <summary>
    /// Interaction logic for MainPage.xaml
    /// </summary>
    public partial class MainPage : Page
    {
        MainWindow m_mainwindow;


        public MainPage(MainWindow mainWindow)
        {
            InitializeComponent();

            m_mainwindow = mainWindow;
            m_mainwindow.BackRequested += M_mainwindow_BackRequested;
        }

        private void M_mainwindow_BackRequested(MainWindow sender, Page content)
        {
            if (content == this)
            {
                m_mainwindow.BackRequested -= M_mainwindow_BackRequested;
                m_mainwindow.GoBack();
            }
        }

        private void btnManageProgrammes_Click(object sender, RoutedEventArgs e)
        {
            m_mainwindow.Navigate(new ManageProgrammesPage(m_mainwindow));
        }

        private void btnManageGrades_Click(object sender, RoutedEventArgs e)
        {
            m_mainwindow.Navigate(new ManageGradesPage(m_mainwindow));
        }

        private void btnManageStudents_Click(object sender, RoutedEventArgs e)
        {
            m_mainwindow.Navigate(new ManageStudentsPage(m_mainwindow));
        }

        private void btnManageBillings_Click(object sender, RoutedEventArgs e)
        {
            m_mainwindow.Navigate(new ManageBillingsPage(m_mainwindow));
        }

        private void btnManagePayments_Click(object sender, RoutedEventArgs e)
        {
            m_mainwindow.Navigate(new ManagePaymentsPage(m_mainwindow));
        }

        private void btnManageTransactions_Click(object sender, RoutedEventArgs e)
        {
            m_mainwindow.Navigate(new ManageTransactionsPage(m_mainwindow));
        }

        private void btnManageSavings_Click(object sender, RoutedEventArgs e)
        {
            m_mainwindow.Navigate(new ManageSavingsPage(m_mainwindow));
        }

        private void btnManageTransactionCategories_Click(object sender, RoutedEventArgs e)
        {
            m_mainwindow.Navigate(new ManageTransactionCategoryPage(m_mainwindow));
        }

        private void btnSavingSummary_Click(object sender, RoutedEventArgs e)
        {
            m_mainwindow.Navigate(new TotalSavingsPage(m_mainwindow));
        }
    }
}
