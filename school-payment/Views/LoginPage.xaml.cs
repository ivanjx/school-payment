﻿using MySql.Data.MySqlClient;
using school_payment.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace school_payment.Views
{
    /// <summary>
    /// Interaction logic for LoginPage.xaml
    /// </summary>
    public partial class LoginPage : Page
    {
        MainWindow m_mainwindow;


        public LoginPage(MainWindow mainwindow)
        {
            InitializeComponent();

            // Filling server address input.
            txtServer.Text = Properties.Settings.Default.ServerAddress;

            m_mainwindow = mainwindow;
        }

        private async void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            btnLogin.IsEnabled = false;

            try
            {
                if (string.IsNullOrEmpty(txtServer.Text))
                {
                    throw new Exception("Server name is empty");
                }

                if (string.IsNullOrEmpty(txtUsername.Text))
                {
                    throw new Exception("Username is empty");
                }

                if (string.IsNullOrEmpty(txtPassword.Password))
                {
                    throw new Exception("Password is empty");
                }

                StaticData.DbConnectionStringBase = string.Format("server = {0};uid = {1};pwd = {2};", txtServer.Text, txtUsername.Text, txtPassword.Password);
                StaticData.DatabaseName = "db_school_payment_smp";

                // Testing connection.
                using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
                {
                    await dbconn.OpenAsync();

                    if (dbconn.State != System.Data.ConnectionState.Open)
                    {
                        throw new Exception("Failed to open database connection");
                    }
                }

                // Connection tested.
                // Clearing user inputs.
                txtUsername.Clear();
                txtPassword.Clear();

                // Saving new server address.
                Properties.Settings.Default.ServerAddress = txtServer.Text;
                Properties.Settings.Default.Save();

                // Redirecting to db selection page.
                m_mainwindow.Navigate(new DbSelectionPage(m_mainwindow));
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to login.\nDetails: " + ex.Message);
            }
            finally
            {
                btnLogin.IsEnabled = true;
            }
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                btnLogin_Click(null, null);
            }
        }
    }
}
