﻿using MySql.Data.MySqlClient;
using school_payment.Commons;
using school_payment.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace school_payment.Views
{
    /// <summary>
    /// Interaction logic for ManageProgrammesPage.xaml
    /// </summary>
    public partial class ManageProgrammesPage : Page
    {
        MainWindow m_mainwindow;
        bool m_canGoBack;
        ObservableCollection<Programme> m_programmes;
        Programme m_programmeBeingEdited;


        public ManageProgrammesPage(MainWindow mainWindow)
        {
            InitializeComponent();

            m_programmes = new ObservableCollection<Programme>();
            lstProgrammes.ItemsSource = m_programmes;

            m_mainwindow = mainWindow;
            m_mainwindow.BackRequested += M_mainwindow_BackRequested;

            Init();
        }

        private void M_mainwindow_BackRequested(MainWindow sender, Page content)
        {
            if (content == this && m_canGoBack)
            {
                m_programmes.Clear();

                m_mainwindow.BackRequested -= M_mainwindow_BackRequested;
                m_mainwindow.GoBack();
            }
        }

        async void Init()
        {
            m_canGoBack = false;
            m_mainwindow.PlayLoadingAnimation();

            try
            {
                await LoadProgrammesAsync();
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to initialize.\nDetails: " + ex.Message);
            }
            finally
            {
                m_canGoBack = true;
                m_mainwindow.StopLoadingAnimation();
            }
        }

        async Task LoadProgrammesAsync()
        {
            m_programmes.Clear();

            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbconn;
                    cmd.CommandText = "SELECT * FROM programmes ORDER BY name ASC";

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            Programme p = new Programme();
                            p.Id = reader.GetInt32(0);
                            p.Name = reader.GetString(1);

                            m_programmes.Add(p);
                        }
                    }
                }
            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            gridProgramme.Visibility = Visibility.Visible;
            m_canGoBack = false;
        }

        private async void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            Programme selitem = lstProgrammes.SelectedItem as Programme;

            if (selitem != null)
            {
                if (!await m_mainwindow.ShowPromptAsync("Confirmation", "Are you sure want to delete selected item?"))
                {
                    return;
                }

                m_mainwindow.PlayLoadingAnimation();
                btnDelete.IsEnabled = false;

                try
                {
                    using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
                    {
                        await dbconn.OpenAsync();

                        using (MySqlCommand cmd = new MySqlCommand())
                        {
                            cmd.Connection = dbconn;
                            cmd.CommandText = "DELETE FROM programmes WHERE id = @id";
                            cmd.Parameters.AddWithValue("id", selitem.Id);
                            await cmd.ExecuteNonQueryAsync();
                        }
                    }

                    m_programmes.Remove(selitem);
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to delete selected item.\nDetails: " + ex.Message);
                }
                finally
                {
                    m_mainwindow.StopLoadingAnimation();
                    btnDelete.IsEnabled = true;
                }
            }
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            m_programmeBeingEdited = lstProgrammes.SelectedItem as Programme;

            if (m_programmeBeingEdited != null)
            {
                txtName_gridProgramme.Text = m_programmeBeingEdited.Name;

                gridProgramme.Visibility = Visibility.Visible;
                m_canGoBack = false;
            }
        }

        private async void btnSave_gridProgramme_Click(object sender, RoutedEventArgs e)
        {
            btnSave_gridProgramme.IsEnabled = false;
            btnCancel_gridProgramme.IsEnabled = false;
            m_mainwindow.PlayLoadingAnimation();

            try
            {
                if (string.IsNullOrEmpty(txtName_gridProgramme.Text))
                {
                    throw new Exception("Name is empty");
                }

                using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
                {
                    await dbconn.OpenAsync();

                    using (MySqlCommand cmd = new MySqlCommand())
                    {
                        cmd.Connection = dbconn;

                        if (m_programmeBeingEdited == null)
                        {
                            cmd.CommandText = "INSERT INTO programmes(name) VALUES(@name)";
                        }
                        else
                        {
                            cmd.CommandText = "UPDATE programmes SET name = @name WHERE id = @id";
                            cmd.Parameters.AddWithValue("id", m_programmeBeingEdited.Id);
                        }

                        cmd.Parameters.AddWithValue("name", txtName_gridProgramme.Text);
                        await cmd.ExecuteNonQueryAsync();
                    }
                }

                await LoadProgrammesAsync();
                btnCancel_gridProgramme_Click(null, null);
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to save changes.\nDetails: " + ex.Message);
            }
            finally
            {
                btnSave_gridProgramme.IsEnabled = true;
                btnCancel_gridProgramme.IsEnabled = true;
                m_mainwindow.StopLoadingAnimation();
            }
        }

        private void btnCancel_gridProgramme_Click(object sender, RoutedEventArgs e)
        {
            m_canGoBack = true;
            m_programmeBeingEdited = null;
            gridProgramme.Visibility = Visibility.Hidden;

            txtName_gridProgramme.Clear();
        }
    }
}
