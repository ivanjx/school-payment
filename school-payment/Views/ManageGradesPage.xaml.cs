﻿using MySql.Data.MySqlClient;
using school_payment.Commons;
using school_payment.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace school_payment.Views
{
    /// <summary>
    /// Interaction logic for ManageGradesPage.xaml
    /// </summary>
    public partial class ManageGradesPage : Page
    {
        MainWindow m_mainwindow;
        bool m_canGoBack;
        ObservableCollection<Grade> m_grades;


        public ManageGradesPage(MainWindow mainWindow)
        {
            InitializeComponent();

            m_grades = new ObservableCollection<Grade>();
            lstGrades.ItemsSource = m_grades;

            m_mainwindow = mainWindow;
            m_mainwindow.BackRequested += M_mainwindow_BackRequested;

            Init();
        }

        private void M_mainwindow_BackRequested(MainWindow sender, Page content)
        {
            if (content == this && m_canGoBack)
            {
                m_grades.Clear();

                m_mainwindow.BackRequested -= M_mainwindow_BackRequested;
                m_mainwindow.GoBack();
            }
        }

        async void Init()
        {
            m_mainwindow.PlayLoadingAnimation();
            m_canGoBack = false;

            try
            {
                await LoadGradesAsync();
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to initalize.\nDetails: " + ex.Message);
            }
            finally
            {
                m_mainwindow.StopLoadingAnimation();
                m_canGoBack = true;
            }
        }

        async Task LoadGradesAsync()
        {
            m_grades.Clear();

            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbconn;
                    cmd.CommandText = "SELECT * FROM grades";

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            Grade g = new Grade();
                            g.Id = reader.GetInt32(0);
                            g.Name = reader.GetString(1);

                            m_grades.Add(g);
                        }
                    }
                }
            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            gridGrade.Visibility = Visibility.Visible;
        }

        private async void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            Grade selitem = lstGrades.SelectedItem as Grade;

            if (selitem != null)
            {
                if (!await m_mainwindow.ShowPromptAsync("Confirmation", "Are you sure want to delete selected item?"))
                {
                    return;
                }

                btnDelete.IsEnabled = false;
                m_mainwindow.PlayLoadingAnimation();

                try
                {
                    using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
                    {
                        await dbconn.OpenAsync();

                        using (MySqlCommand cmd = new MySqlCommand())
                        {
                            cmd.Connection = dbconn;
                            cmd.CommandText = "DELETE FROM grades WHERE id = @id";
                            cmd.Parameters.AddWithValue("id", selitem.Id);
                            await cmd.ExecuteNonQueryAsync();
                        }
                    }

                    m_grades.Remove(selitem);
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to delete selected item.\nDetails: " + ex.Message);
                }
                finally
                {
                    btnDelete.IsEnabled = true;
                    m_mainwindow.StopLoadingAnimation();
                }
            }
        }

        private async void btnSave_gridGrade_Click(object sender, RoutedEventArgs e)
        {
            btnSave_gridGrade.IsEnabled = false;
            btnCancel_gridGrade.IsEnabled = false;
            m_mainwindow.PlayLoadingAnimation();

            try
            {
                if (string.IsNullOrEmpty(txtName_gridGrade.Text))
                {
                    throw new Exception("Name is empty");
                }

                using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
                {
                    await dbconn.OpenAsync();

                    using (MySqlCommand cmd = new MySqlCommand())
                    {
                        cmd.Connection = dbconn;
                        cmd.CommandText = "INSERT INTO grades(name) VALUES(@name)";
                        cmd.Parameters.AddWithValue("name", txtName_gridGrade.Text);
                        await cmd.ExecuteNonQueryAsync();
                    }
                }

                await LoadGradesAsync();
                btnCancel_gridGrade_Click(null, null);
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to save changes.\nDetails: " + ex.Message);
            }
            finally
            {
                btnSave_gridGrade.IsEnabled = true;
                btnCancel_gridGrade.IsEnabled = true;
                m_mainwindow.StopLoadingAnimation();
            }
        }

        private void btnCancel_gridGrade_Click(object sender, RoutedEventArgs e)
        {
            gridGrade.Visibility = Visibility.Hidden;
            txtName_gridGrade.Clear();
        }
    }
}
