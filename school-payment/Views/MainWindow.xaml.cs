﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls.Dialogs;
using System.Globalization;

namespace school_payment.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public delegate void BackRequestedHandler(MainWindow sender, Page content);
        public event BackRequestedHandler BackRequested;


        public MainWindow()
        {
            InitializeComponent();

            // Disabling backspace actions.
            NavigationCommands.BrowseBack.InputGestures.Clear();
            NavigationCommands.BrowseForward.InputGestures.Clear();

            // Changing culture.
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("id-ID");

            // Navigating to login page.
            Navigate(new LoginPage(this));
        }

        public void ShowBackButton()
        {
            LeftWindowCommands.Width = btnBack.Width;
        }

        public void HideBackButton()
        {
            LeftWindowCommands.Width = 0;
        }

        public void Navigate(Page page)
        {
            frmMain.Navigate(page);
        }

        public void GoBack()
        {
            if (frmMain.CanGoBack)
            {
                frmMain.GoBack();
            }
        }

        public async void ShowMessage(string title, string message)
        {
            await this.ShowMessageAsync(title, message);
        }

        public async Task<bool> ShowPromptAsync(string title, string message)
        {
            return await this.ShowMessageAsync(title, message, MessageDialogStyle.AffirmativeAndNegative) == MessageDialogResult.Affirmative;
        }

        public void PlayLoadingAnimation()
        {
            pgsLoading.IsActive = true;
        }

        public void StopLoadingAnimation()
        {
            pgsLoading.IsActive = false;
        }

        private void frmMain_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            Page content = frmMain.Content as Page;

            if (content != null)
            {
                this.Title = "School Payment System - " + content.Title;
            }

            if (frmMain.CanGoBack)
            {
                ShowBackButton();
            }
            else
            {
                HideBackButton();
            }
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            BackRequested?.Invoke(this, frmMain.Content as Page);
        }
    }
}
