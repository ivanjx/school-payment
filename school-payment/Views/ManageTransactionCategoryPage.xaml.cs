﻿using MySql.Data.MySqlClient;
using school_payment.Commons;
using school_payment.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace school_payment.Views
{
    /// <summary>
    /// Interaction logic for ManageTransactionCategoryPage.xaml
    /// </summary>
    public partial class ManageTransactionCategoryPage : Page
    {
        bool m_canGoBack;
        MainWindow m_mainwindow;
        ObservableCollection<TransactionCategory> m_categories;


        public ManageTransactionCategoryPage(MainWindow mainwindow)
        {
            InitializeComponent();

            m_categories = new ObservableCollection<TransactionCategory>();
            lstCategories.ItemsSource = m_categories;

            m_mainwindow = mainwindow;
            m_mainwindow.BackRequested += M_mainwindow_BackRequested;

            Init();
        }

        private void M_mainwindow_BackRequested(MainWindow sender, Page content)
        {
            if (content == this && m_canGoBack)
            {
                m_categories.Clear();
                m_mainwindow.BackRequested -= M_mainwindow_BackRequested;
                m_mainwindow.GoBack();
            }
        }

        async void Init()
        {
            m_canGoBack = false;

            try
            {
                await LoadCategoriesAsync();
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to load categories.\nDetails: " + ex.Message);
            }
            finally
            {
                m_canGoBack = true;
            }
        }

        async Task LoadCategoriesAsync()
        {
            m_categories.Clear();

            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbconn;
                    cmd.CommandText = "SELECT * FROM transaction_categories";

                    using (MySqlDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            TransactionCategory tc = new TransactionCategory();
                            tc.Id = reader.GetInt32(0);
                            tc.Name = reader.GetString(1);

                            m_categories.Add(tc);
                        }
                    }
                }
            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            gridCategory.Visibility = Visibility.Visible;
            m_canGoBack = false;
        }

        private async void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            TransactionCategory selitem = lstCategories.SelectedItem as TransactionCategory;

            if (selitem != null)
            {
                if (!await m_mainwindow.ShowPromptAsync("Confirmation", "Are you sure want to delete selected item?"))
                {
                    return;
                }

                btnDelete.IsEnabled = false;

                try
                {
                    using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
                    {
                        await dbconn.OpenAsync();

                        using (MySqlCommand cmd = new MySqlCommand())
                        {
                            cmd.Connection = dbconn;
                            cmd.CommandText = "DELETE FROM transaction_categories WHERE id = @id";
                            cmd.Parameters.AddWithValue("id", selitem.Id);

                            await cmd.ExecuteNonQueryAsync();
                        }
                    }

                    m_categories.Remove(selitem);
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to delete selected item.\nDetails: " + ex.Message);
                }
                finally
                {
                    btnDelete.IsEnabled = true;
                }
            }
        }

        private async void btnSave_gridCategory_Click(object sender, RoutedEventArgs e)
        {
            btnSave_gridCategory.IsEnabled = false;
            btnCancel_gridCategory.IsEnabled = false;

            try
            {
                // validating.
                if (string.IsNullOrEmpty(txtName_gridCategory.Text))
                {
                    throw new Exception("Name is empty");
                }

                // saving.
                using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
                {
                    await dbconn.OpenAsync();

                    using (MySqlCommand cmd = new MySqlCommand())
                    {
                        cmd.Connection = dbconn;
                        cmd.CommandText = "INSERT INTO transaction_categories(name) VALUES(@name)";
                        cmd.Parameters.AddWithValue("name", txtName_gridCategory.Text);

                        await cmd.ExecuteNonQueryAsync();
                    }
                }

                // done.
                await LoadCategoriesAsync();
                btnCancel_gridCategory_Click(null, null);
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to save changes.\nDetails: " + ex.Message);
            }
            finally
            {
                btnSave_gridCategory.IsEnabled = true;
                btnCancel_gridCategory.IsEnabled = true;
            }
        }

        private void btnCancel_gridCategory_Click(object sender, RoutedEventArgs e)
        {
            txtName_gridCategory.Clear();
            gridCategory.Visibility = Visibility.Hidden;
            m_canGoBack = true;
        }
    }
}
