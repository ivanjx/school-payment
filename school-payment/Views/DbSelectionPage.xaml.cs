﻿using school_payment.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace school_payment.Views
{
    /// <summary>
    /// Interaction logic for DbSelectionPage.xaml
    /// </summary>
    public partial class DbSelectionPage : Page
    {
        MainWindow m_mainwindow;


        public DbSelectionPage(MainWindow mainwindow)
        {
            InitializeComponent();

            cmbDb.Items.Add("SMP");
            cmbDb.Items.Add("SMK");
            cmbDb.Items.Add("YAYASAN");

            m_mainwindow = mainwindow;
            m_mainwindow.BackRequested += M_mainwindow_BackRequested;
        }

        private void M_mainwindow_BackRequested(MainWindow sender, Page content)
        {
            if (content == this)
            {
                m_mainwindow.BackRequested -= M_mainwindow_BackRequested;
                m_mainwindow.GoBack();
            }
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            string selitem = cmbDb.SelectedItem as string;

            if (selitem != null)
            {
                if (selitem == "SMP")
                {
                    StaticData.DatabaseName = "db_school_payment_smp";
                }
                else if (selitem == "SMK")
                {
                    StaticData.DatabaseName = "db_school_payment_smk";
                }
                else
                {
                    StaticData.DatabaseName = "db_school_payment_yayasan";
                }
            }

            // Go to main page.
            m_mainwindow.Navigate(new MainPage(m_mainwindow));
        }
    }
}
