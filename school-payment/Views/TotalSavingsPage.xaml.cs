﻿using Microsoft.Reporting.WinForms;
using MySql.Data.MySqlClient;
using school_payment.Commons;
using school_payment.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace school_payment.Views
{
    /// <summary>
    /// Interaction logic for TotalSavingsPage.xaml
    /// </summary>
    public partial class TotalSavingsPage : Page
    {
        bool m_canGoBack;
        MainWindow m_mainwindow;
        ObservableCollection<SavingSummary> m_summary;


        public TotalSavingsPage(MainWindow mainwindow)
        {
            InitializeComponent();

            m_summary = new ObservableCollection<SavingSummary>();
            lstSavingsSummary.ItemsSource = m_summary;

            m_mainwindow = mainwindow;
            m_mainwindow.BackRequested += M_mainwindow_BackRequested;

            Init();
        }

        private void M_mainwindow_BackRequested(MainWindow sender, Page content)
        {
            if (content == this && m_canGoBack)
            {
                m_mainwindow.BackRequested -= M_mainwindow_BackRequested;
                m_mainwindow.GoBack();
            }
        }

        async void Init()
        {
            m_canGoBack = false;

            try
            {
                await LoadSavingSummaryAsync();
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to load items.\nDetails: " + ex.Message);
            }
            finally
            {
                m_canGoBack = true;
            }
        }

        async Task LoadSavingSummaryAsync()
        {
            m_summary.Clear();

            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbconn;
                    cmd.CommandText = "SELECT DISTINCT student_id FROM savings";

                    using (MySqlDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            Student student = await Loader.GetStudentByIdAsync(reader.GetInt32(0));

                            // Getting total amount.
                            int amount = await GetTotalSavingAmount(student);

                            // getting last date.
                            DateTime lastdate = await GetLastSavingTransactionTimeAsync(student);

                            // Adding.
                            SavingSummary ss = new SavingSummary();
                            ss.StudentId = student.StudentId;
                            ss.StudentName = student.Name;
                            ss.LastTransactionTime = lastdate;
                            ss.TotalAmount = amount;

                            m_summary.Add(ss);
                        }
                    }
                }
            }
        }

        async Task<int> GetTotalSavingAmount(Student student)
        {
            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    int outAmount = 0, 
                        inAmount = 0;

                    cmd.Connection = dbconn;
                    cmd.CommandText = "SELECT SUM(amount) FROM savings WHERE student_id = @studentId AND is_out = 0";
                    cmd.Parameters.AddWithValue("studentId", student.Id);

                    using (MySqlDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        if (await reader.ReadAsync())
                        {
                            try
                            {
                                inAmount = reader.GetInt32(0);
                            }
                            catch
                            {
                                // Could be null.
                                inAmount = 0;
                            }
                        }
                    }

                    cmd.CommandText = "SELECT SUM(amount) FROM savings WHERE student_id = @studentId AND is_out = 1";

                    using (MySqlDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        if (await reader.ReadAsync())
                        {
                            try
                            {
                                outAmount = reader.GetInt32(0);
                            }
                            catch
                            {
                                // Could be null.
                                outAmount = 0;
                            }
                        }
                    }

                    return inAmount - outAmount;
                }
            }
        }

        async Task<DateTime> GetLastSavingTransactionTimeAsync(Student student)
        {
            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbconn;
                    cmd.CommandText = "SELECT MAX(t_time) FROM savings WHERE student_id = @studentId";
                    cmd.Parameters.AddWithValue("studentId", student.Id);

                    using (MySqlDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        if (await reader.ReadAsync())
                        {
                            return reader.GetDateTime(0);
                        }

                        return DateTime.MinValue;
                    }
                }
            }
        }

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            int subtotal = 0;

            for (int i = 0; i < m_summary.Count; ++i)
            {
                subtotal += m_summary[i].TotalAmount;
            }

            ReportDataSource rds = new ReportDataSource("SavingSummaryDataSet", m_summary);

            List<ReportParameter> reportParameters = new List<ReportParameter>();
            reportParameters.Add(new ReportParameter("SubTotalString", subtotal.ToString("C0")));
            reportParameters.Add(new ReportParameter("DateString", DateTime.Now.ToString("d MMMM yyyy")));

            PrintWindow p = new PrintWindow("Resources\\Templates\\SavingSummaryReport.rdlc", rds, reportParameters);
            p.Show();
        }
    }
}
