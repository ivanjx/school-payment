﻿using Microsoft.Reporting.WinForms;
using MySql.Data.MySqlClient;
using school_payment.Commons;
using school_payment.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace school_payment.Views
{
    /// <summary>
    /// Interaction logic for ManageSavings.xaml
    /// </summary>
    public partial class ManageSavingsPage : Page
    {
        // Page definition.
        MainWindow m_mainwindow;
        ObservableCollection<Saving> m_savings;
        Student m_selStudent;
        int m_balance;


        public ManageSavingsPage(MainWindow mainwindow)
        {
            InitializeComponent();

            m_savings = new ObservableCollection<Saving>();
            lstSavings.ItemsSource = m_savings;

            cmbType_gridSaving.Items.Add("IN");
            cmbType_gridSaving.Items.Add("OUT");

            m_mainwindow = mainwindow;
            m_mainwindow.BackRequested += M_mainwindow_BackRequested;
        }

        private void M_mainwindow_BackRequested(MainWindow sender, Page content)
        {
            if (content == this)
            {
                m_mainwindow.BackRequested -= M_mainwindow_BackRequested;
                m_mainwindow.GoBack();
            }
        }

        async Task LoadSavingsOfStudentAsync()
        {
            if (m_selStudent == null)
            {
                throw new Exception("Student not selected");
            }

            m_savings.Clear();
            m_balance = 0;

            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbconn;
                    cmd.CommandText = "SELECT t_time, amount, is_out FROM savings WHERE student_id = @id ORDER BY t_time DESC";
                    cmd.Parameters.AddWithValue("id", m_selStudent.Id);

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            // Getting saving info.
                            Saving sv = new Saving();
                            sv.Student = m_selStudent;
                            sv.TTime = reader.GetDateTime(0);
                            sv.Amount = reader.GetInt32(1);
                            sv.IsOut = reader.GetInt32(2) == 1;

                            m_savings.Add(sv);

                            if (sv.IsOut)
                            {
                                m_balance -= sv.Amount;
                            }
                            else
                            {
                                m_balance += sv.Amount;
                            }
                        }
                    }
                }
            }

            txtBalance.Text = m_balance.ToString("C0");
        }

        async Task<(int pre, int post)> GetPrePostBalanceAsync(Saving saving)
        {
            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    int add, sub;

                    // Getting all in transactions.
                    cmd.Connection = dbconn;
                    cmd.CommandText = "SELECT COALESCE(SUM(amount),0) FROM savings WHERE student_id = @id AND t_time < @point AND is_out = 0";
                    cmd.Parameters.AddWithValue("id", m_selStudent.Id);
                    cmd.Parameters.AddWithValue("point", saving.TTime);

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        if (await reader.ReadAsync())
                        {
                            add = reader.GetInt32(0);
                        }
                        else
                        {
                            throw new Exception("Unable to calculate pre and post balances");
                        }
                    }

                    // Getting all out transactions.
                    cmd.CommandText = "SELECT COALESCE(SUM(amount),0) FROM savings WHERE student_id = @id AND t_time < @point AND is_out = 1";

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        if (await reader.ReadAsync())
                        {
                            sub = reader.GetInt32(0);
                        }
                        else
                        {
                            throw new Exception("Unable to calculate pre and post balances");
                        }
                    }

                    // Calculating.
                    int pre = add - sub;
                    int post;

                    if (saving.IsOut)
                    {
                        post = pre - saving.Amount;
                    }
                    else
                    {
                        post = pre + saving.Amount;
                    }

                    return (pre, post);
                }
            }
        }

        private async void btnSearchStudentById_Click(object sender, RoutedEventArgs e)
        {
            btnSearchStudentById.IsEnabled = false;
            m_mainwindow.PlayLoadingAnimation();

            // Clearing last record.
            m_selStudent = null;
            txtName.Clear();
            txtGradeName.Clear();
            txtProgrammeName.Clear();
            txtBalance.Clear();

            try
            {
                // Checking user input.
                if (string.IsNullOrEmpty(txtId.Text))
                {
                    throw new Exception("Student ID is empty");
                }

                // Loading student info.
                m_selStudent = await Loader.GetStudentByStudentIdAsync(txtId.Text);
                txtName.Text = m_selStudent.Name;
                txtGradeName.Text = m_selStudent.Grade.Name;
                txtProgrammeName.Text = m_selStudent.Programme.Name;

                // Loading savings record.
                await LoadSavingsOfStudentAsync();
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to load data.\nDetails: " + ex.Message);
            }
            finally
            {
                btnSearchStudentById.IsEnabled = true;
                m_mainwindow.StopLoadingAnimation();
            }
        }

        private async void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            Saving selitem = lstSavings.SelectedItem as Saving;

            if (selitem != null)
            {
                bool proceed = await m_mainwindow.ShowPromptAsync("Confirmation", "Are you sure want to delete selected record?");

                if (!proceed)
                {
                    return;
                }

                btnDelete.IsEnabled = false;
                m_mainwindow.PlayLoadingAnimation();

                try
                {
                    using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
                    {
                        await dbconn.OpenAsync();

                        using (MySqlCommand cmd = new MySqlCommand())
                        {
                            cmd.Connection = dbconn;
                            cmd.CommandText = "DELETE FROM savings WHERE student_id = @id AND t_time = @ttime";
                            cmd.Parameters.AddWithValue("id", selitem.Student.Id);
                            cmd.Parameters.AddWithValue("ttime", selitem.TTime);
                            await cmd.ExecuteNonQueryAsync();
                        }
                    }

                    // Reloading list.
                    await LoadSavingsOfStudentAsync();
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to delete selected item.\nDetails: " + ex.Message);
                }
                finally
                {
                    btnDelete.IsEnabled = true;
                    m_mainwindow.StopLoadingAnimation();
                }
            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            gridSaving.Visibility = Visibility.Visible;
        }

        private async void btnSave_gridSaving_Click(object sender, RoutedEventArgs e)
        {
            btnSave_gridSaving.IsEnabled = false;
            btnCancel_gridSaving.IsEnabled = false;
            m_mainwindow.PlayLoadingAnimation();

            try
            {
                // Checking user inputs.
                int amount = int.Parse(txtAmount_gridSaving.Text);

                if (cmbType_gridSaving.SelectedItem == null)
                {
                    throw new Exception("Type not selected");
                }

                bool isOut = (cmbType_gridSaving.SelectedItem as string) == "OUT";

                // Checking validity.
                if (isOut && amount > m_balance)
                {
                    throw new Exception("Insuficient balance");
                }

                // Saving to database.
                using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
                {
                    await dbconn.OpenAsync();

                    using (MySqlCommand cmd = new MySqlCommand())
                    {
                        cmd.Connection = dbconn;
                        cmd.CommandText = "INSERT INTO savings VALUES(@studentId, @ttime, @amount, @isOut)";
                        cmd.Parameters.AddWithValue("studentId", m_selStudent.Id);
                        cmd.Parameters.AddWithValue("ttime", DateTime.Now);
                        cmd.Parameters.AddWithValue("amount", amount);
                        cmd.Parameters.AddWithValue("isOut", isOut ? 1 : 0);
                        await cmd.ExecuteNonQueryAsync();
                    }
                }

                // Reloading savings list.
                await LoadSavingsOfStudentAsync();

                // Done.
                btnCancel_gridSaving_Click(null, null);
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to save changes.\nDetails: " + ex.Message);
            }
            finally
            {
                btnSave_gridSaving.IsEnabled = true;
                btnCancel_gridSaving.IsEnabled = true;
                m_mainwindow.StopLoadingAnimation();
            }
        }

        private void btnCancel_gridSaving_Click(object sender, RoutedEventArgs e)
        {
            gridSaving.Visibility = Visibility.Hidden;

            txtAmount_gridSaving.Clear();
            cmbType_gridSaving.SelectedItem = null;
        }

        private async void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            Saving selitem = lstSavings.SelectedItem as Saving;

            if (selitem != null)
            {
                btnPrint.IsEnabled = false;

                try
                {
                    // Calculating pre amount.
                    var balances = await GetPrePostBalanceAsync(selitem);

                    List<ReportParameter> reportParameters = new List<ReportParameter>();
                    reportParameters.Add(new ReportParameter("PayerName", m_selStudent.Name));
                    reportParameters.Add(new ReportParameter("Amount", selitem.Amount.ToString("C")));
                    reportParameters.Add(new ReportParameter("PreAmount", balances.pre.ToString("C")));
                    reportParameters.Add(new ReportParameter("PostAmount", balances.post.ToString("C")));
                    reportParameters.Add(new ReportParameter("TransactionType", selitem.IsOut ? "Keluar" : "Masuk"));
                    reportParameters.Add(new ReportParameter("DateString", DateTime.Now.ToString("d MMMM yyyy")));

                    PrintWindow p = new PrintWindow("Resources\\Templates\\SavingReport.rdlc", null, reportParameters);
                    p.Show();
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to print selected record.\nDetails: " + ex.Message);
                }
                finally
                {
                    btnPrint.IsEnabled = true;
                }
            }
        }
    }
}
