﻿using Microsoft.Reporting.WinForms;
using MySql.Data.MySqlClient;
using school_payment.Commons;
using school_payment.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace school_payment.Views
{
    /// <summary>
    /// Interaction logic for ManagePaymentsPage.xaml
    /// </summary>
    public partial class ManagePaymentsPage : Page
    {
        // View models.
        class PaymentViewModel : INotifyPropertyChanged
        {
            int m_paidAmount;
            bool m_isChecked;
            DateTime? m_lastPaidTime;

            public event PropertyChangedEventHandler PropertyChanged;

            public BillingLink BillingLink { get; set; }

            public bool IsChecked
            {
                get
                {
                    return m_isChecked;
                }
                set
                {
                    m_isChecked = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsChecked"));
                }
            }

            public string BillingTypeName
            {
                get
                {
                    return BillingLink.BillingType.Name;
                }
            }

            public int TotalAmount
            {
                get
                {
                    return BillingLink.Amount;
                }
            }

            public int PaidAmount
            {
                get
                {
                    return m_paidAmount;
                }
            }

            public int RemainingAmount
            {
                get
                {
                    return BillingLink.Amount - PaidAmount;
                }
            }

            public DateTime? LastPaidTime
            {
                get
                {
                    return m_lastPaidTime;
                }
            }


            public PaymentViewModel(BillingLink link, int paidAmount, DateTime? lastPaidtime)
            {
                m_paidAmount = paidAmount;
                m_lastPaidTime = lastPaidtime;
                BillingLink = link;
            }

            public override bool Equals(object obj)
            {
                PaymentViewModel comp = obj as PaymentViewModel;

                if (comp != null)
                {
                    return BillingLink.Equals(comp.BillingLink);
                }

                return false;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }

        class PaymentHistoryViewModel : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            bool m_isChecked;

            public bool IsChecked
            {
                get
                {
                    return m_isChecked;
                }
                set
                {
                    m_isChecked = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsChecked"));
                }
            }

            public Transaction Transaction
            {
                get;
                private set;
            }


            public PaymentHistoryViewModel(Transaction transaction)
            {
                Transaction = transaction;
            }

            public override bool Equals(object obj)
            {
                PaymentHistoryViewModel comp = obj as PaymentHistoryViewModel;

                if (comp != null)
                {
                    return Transaction.Equals(comp.Transaction);
                }

                return false;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }

        // Page definition.
        MainWindow m_mainwindow;
        bool m_canGoBack;
        Student m_selectedStudent;
        ObservableCollection<PaymentViewModel> m_oncePayments;
        ObservableCollection<PaymentViewModel> m_annualPayments;
        ObservableCollection<PaymentViewModel> m_semesterPayments;
        ObservableCollection<PaymentViewModel> m_monthPayments;
        ObservableCollection<PaymentHistoryViewModel> m_paymentHistory;
        PaymentViewModel m_paymentBeingPaid;


        public ManagePaymentsPage(MainWindow mainwindow)
        {
            InitializeComponent();

            m_oncePayments = new ObservableCollection<PaymentViewModel>();
            lstOnce.ItemsSource = m_oncePayments;

            m_annualPayments = new ObservableCollection<PaymentViewModel>();
            lstAnnually.ItemsSource = m_annualPayments;

            m_semesterPayments = new ObservableCollection<PaymentViewModel>();
            lstSemester.ItemsSource = m_semesterPayments;

            m_monthPayments = new ObservableCollection<PaymentViewModel>();
            lstMonthly.ItemsSource = m_monthPayments;

            m_paymentHistory = new ObservableCollection<PaymentHistoryViewModel>();
            lstHistory.ItemsSource = m_paymentHistory;

            m_mainwindow = mainwindow;
            m_mainwindow.BackRequested += M_mainwindow_BackRequested;

            Init();
        }

        private void M_mainwindow_BackRequested(MainWindow sender, Page content)
        {
            if (content == this && m_canGoBack)
            {
                m_mainwindow.BackRequested -= M_mainwindow_BackRequested;
                
                (cmbMonths_Monthly.ItemsSource as List<string>).Clear();
                (cmbMonth_gridPayment.ItemsSource as List<string>).Clear();

                (cmbSemester_Semester.ItemsSource as List<string>).Clear();
                (cmbSemester_gridPayment.ItemsSource as List<string>).Clear();

                (cmbYears_Annually.ItemsSource as List<int>).Clear();
                (cmbYears_Semester.ItemsSource as List<int>).Clear();
                (cmbYears_Monthly.ItemsSource as List<int>).Clear();
                (cmbYear_gridPayment.ItemsSource as List<int>).Clear();

                m_oncePayments.Clear();
                m_annualPayments.Clear();
                m_semesterPayments.Clear();
                m_monthPayments.Clear();

                m_mainwindow.GoBack();
            }
        }

        void Init()
        {
            // Loading 7 years back and forth.
            int yearRange = 7;

            List<int> years = new List<int>();
            int year = DateTime.Now.Year - yearRange;

            for (int i = 0; i < yearRange * 2 + 1; ++i)
            {
                years.Add(year);
                ++year;
            }

            cmbYears_Annually.ItemsSource = years;
            cmbYears_Monthly.ItemsSource = years;
            cmbYears_Semester.ItemsSource = years;
            cmbYear_gridPayment.ItemsSource = years;

            // Loading semesters.
            List<string> semesters = new List<string>();
            semesters.Add(StaticData.FIRST_SEMESTER);
            semesters.Add(StaticData.SECOND_SEMESTER);

            cmbSemester_Semester.ItemsSource = semesters;
            cmbSemester_gridPayment.ItemsSource = semesters;

            // Loading months.
            List<string> months = new List<string>();
            months.AddRange(StaticData.MONTHS_NAME);

            cmbMonths_Monthly.ItemsSource = months;
            cmbMonth_gridPayment.ItemsSource = months;

            // Done.
            m_canGoBack = true;
        }

        async Task LoadOncePaymentsAsync(Student student)
        {
            m_oncePayments.Clear();

            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                // Getting billing types of selected programme.
                List<BillingLink> links = new List<BillingLink>();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbconn;
                    cmd.CommandText = "SELECT bt.id, bt.name, bl.amount ";
                    cmd.CommandText += "FROM billing_types bt, billing_links bl ";
                    cmd.CommandText += "WHERE bt.id = bl.billing_type_id AND bt.day_interval = @dint AND bl.programme_id = @programmeId";
                    cmd.Parameters.AddWithValue("programmeId", student.Programme.Id);
                    cmd.Parameters.AddWithValue("dint", StaticData.PERIOD_ONCE);

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            BillingType bt = new BillingType();
                            bt.Id = reader.GetInt32(0);
                            bt.Name = reader.GetString(1);
                            bt.DayInterval = StaticData.PERIOD_ONCE;

                            BillingLink bl = new BillingLink();
                            bl.BillingType = bt;
                            bl.Amount = reader.GetInt32(2);
                            bl.Programme = student.Programme;

                            links.Add(bl);
                        }
                    }
                }

                // Getting paid amount for each billing types.
                for (int i = 0; i < links.Count; ++i)
                {
                    using (MySqlCommand cmd = new MySqlCommand())
                    {
                        cmd.Connection = dbconn;
                        cmd.CommandText = "SELECT SUM(t.amount), MAX(t.t_time)";
                        cmd.CommandText += "FROM student_payments sp, transactions t ";
                        cmd.CommandText += "WHERE t.id = sp.transaction_id AND sp.billing_type_id = @btid AND sp.student_id = @sid";
                        cmd.Parameters.AddWithValue("btid", links[i].BillingType.Id);
                        cmd.Parameters.AddWithValue("sid", student.Id);

                        int paidAmount = 0;
                        DateTime? lastPaidTime = null;

                        using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            if (await reader.ReadAsync())
                            {
                                paidAmount = await reader.IsDBNullAsync(0) ? 0 : reader.GetInt32(0);
                                lastPaidTime = await reader.IsDBNullAsync(1) ? default(DateTime?) : reader.GetDateTime(1);
                            }
                        }

                        PaymentViewModel vm = new PaymentViewModel(links[i], paidAmount, lastPaidTime);
                        m_oncePayments.Add(vm);
                    }
                }
            }
        }

        async Task LoadAnnualPaymentsAsync(Student student, int year)
        {
            m_annualPayments.Clear();

            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                // Getting billing types of selected programme.
                List<BillingLink> links = new List<BillingLink>();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbconn;
                    cmd.CommandText = "SELECT bt.id, bt.name, bl.amount ";
                    cmd.CommandText += "FROM billing_types bt, billing_links bl ";
                    cmd.CommandText += "WHERE bt.id = bl.billing_type_id AND bt.day_interval = @dint AND bl.programme_id = @programmeId";
                    cmd.Parameters.AddWithValue("dint", StaticData.PERIOD_ANNUALLY);
                    cmd.Parameters.AddWithValue("programmeId", student.Programme.Id);

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            BillingType bt = new BillingType();
                            bt.Id = reader.GetInt32(0);
                            bt.Name = reader.GetString(1);
                            bt.DayInterval = StaticData.PERIOD_ANNUALLY;

                            BillingLink bl = new BillingLink();
                            bl.BillingType = bt;
                            bl.Amount = reader.GetInt32(2);
                            bl.Programme = student.Programme;

                            links.Add(bl);
                        }
                    }
                }

                // Calculating paid amounts.
                for (int i = 0; i < links.Count; ++i)
                {
                    using (MySqlCommand cmd = new MySqlCommand())
                    {
                        cmd.Connection = dbconn;
                        cmd.CommandText = "SELECT SUM(t.amount), MAX(t.t_time) ";
                        cmd.CommandText += "FROM transactions t, student_payments sp ";
                        cmd.CommandText += "WHERE sp.transaction_id = t.id AND sp.billing_type_id = @btid AND sp.student_id = @sid AND year = @year";
                        cmd.Parameters.AddWithValue("btid", links[i].BillingType.Id);
                        cmd.Parameters.AddWithValue("sid", student.Id);
                        cmd.Parameters.AddWithValue("year", year);

                        int paidAmount = 0;
                        DateTime? lastPaidTime = null;

                        using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            if (await reader.ReadAsync())
                            {
                                paidAmount = await reader.IsDBNullAsync(0) ? 0 : reader.GetInt32(0);
                                lastPaidTime = await reader.IsDBNullAsync(1) ? default(DateTime?) : reader.GetDateTime(1);
                            }
                        }

                        PaymentViewModel vm = new PaymentViewModel(links[i], paidAmount, lastPaidTime);
                        m_annualPayments.Add(vm);
                    }
                }
            }
        }

        async Task LoadSemesterPaymentsAsync(Student student, int year, string semester)
        {
            m_semesterPayments.Clear();

            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                // Getting all payment type of semester.
                List<BillingLink> links = new List<BillingLink>();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbconn;
                    cmd.CommandText = "SELECT bt.id, bt.name, bl.amount ";
                    cmd.CommandText += "FROM billing_types bt, billing_links bl ";
                    cmd.CommandText += "WHERE bt.id = bl.billing_type_id AND bt.day_interval = @dint AND bl.programme_id = @programmeId";
                    cmd.Parameters.AddWithValue("dint", StaticData.PERIOD_SEMESTER);
                    cmd.Parameters.AddWithValue("programmeId", student.Programme.Id);

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            BillingType bt = new BillingType();
                            bt.Id = reader.GetInt32(0);
                            bt.Name = reader.GetString(1);
                            bt.DayInterval = StaticData.PERIOD_SEMESTER;

                            BillingLink bl = new BillingLink();
                            bl.BillingType = bt;
                            bl.Amount = reader.GetInt32(2);
                            bl.Programme = student.Programme;

                            links.Add(bl);
                        }
                    }
                }

                // Getting paid amounts.
                for (int i = 0; i < links.Count; ++i)
                {
                    using (MySqlCommand cmd = new MySqlCommand())
                    {
                        cmd.Connection = dbconn;
                        cmd.CommandText = "SELECT SUM(t.amount), MAX(t.t_time) ";
                        cmd.CommandText += "FROM transactions t, student_payments sp ";
                        cmd.CommandText += "WHERE sp.transaction_id = t.id AND sp.billing_type_id = @btid AND sp.student_id = @sid AND sp.year = @year AND sp.semester = @semester";
                        cmd.Parameters.AddWithValue("btid", links[i].BillingType.Id);
                        cmd.Parameters.AddWithValue("sid", student.Id);
                        cmd.Parameters.AddWithValue("year", year);
                        cmd.Parameters.AddWithValue("semester", semester);

                        int paidAmount = 0;
                        DateTime? lastPaidTime = null;

                        using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            if (await reader.ReadAsync())
                            {
                                paidAmount = await reader.IsDBNullAsync(0) ? 0 : reader.GetInt32(0);
                                lastPaidTime = await reader.IsDBNullAsync(1) ? default(DateTime?) : reader.GetDateTime(1);
                            }
                        }

                        PaymentViewModel vm = new PaymentViewModel(links[i], paidAmount, lastPaidTime);
                        m_semesterPayments.Add(vm);
                    }
                }
            }
        }

        async Task LoadMonthPaymentsAsync(Student student, int year, string month)
        {
            m_monthPayments.Clear();

            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                // Getting all payment type of month.
                List<BillingLink> links = new List<BillingLink>();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbconn;
                    cmd.CommandText = "SELECT bt.id, bt.name, bl.amount ";
                    cmd.CommandText += "FROM billing_types bt, billing_links bl ";
                    cmd.CommandText += "WHERE bt.id = bl.billing_type_id AND bt.day_interval = @dint AND bl.programme_id = @programmeId";
                    cmd.Parameters.AddWithValue("dint", StaticData.PERIOD_MONTHLY);
                    cmd.Parameters.AddWithValue("programmeId", student.Programme.Id);

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            BillingType bt = new BillingType();
                            bt.Id = reader.GetInt32(0);
                            bt.Name = reader.GetString(1);
                            bt.DayInterval = StaticData.PERIOD_MONTHLY;

                            BillingLink bl = new BillingLink();
                            bl.BillingType = bt;
                            bl.Amount = reader.GetInt32(2);
                            bl.Programme = student.Programme;

                            links.Add(bl);
                        }
                    }
                }

                // Getting paid amounts.
                for (int i = 0; i < links.Count; ++i)
                {
                    using (MySqlCommand cmd = new MySqlCommand())
                    {
                        cmd.Connection = dbconn;
                        cmd.CommandText = "SELECT SUM(t.amount), MAX(t.t_time) ";
                        cmd.CommandText += "FROM transactions t, student_payments sp ";
                        cmd.CommandText += "WHERE sp.transaction_id = t.id AND sp.billing_type_id = @btid AND sp.student_id = @sid AND sp.year = @year AND sp.month = @month";
                        cmd.Parameters.AddWithValue("btid", links[i].BillingType.Id);
                        cmd.Parameters.AddWithValue("sid", student.Id);
                        cmd.Parameters.AddWithValue("year", year);
                        cmd.Parameters.AddWithValue("month", month);

                        int paidAmount = 0;
                        DateTime? lastPaidTime = null;

                        using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            if (await reader.ReadAsync())
                            {
                                paidAmount = await reader.IsDBNullAsync(0) ? 0 : reader.GetInt32(0);
                                lastPaidTime = await reader.IsDBNullAsync(1) ? default(DateTime?) : reader.GetDateTime(1);
                            }
                        }

                        PaymentViewModel vm = new PaymentViewModel(links[i], paidAmount, lastPaidTime);
                        m_monthPayments.Add(vm);
                    }
                }
            }
        }

        async Task LoadPaymentHistoryAsync(Student student)
        {
            m_paymentHistory.Clear();

            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                // Getting all transactions of a student.
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbconn;
                    cmd.CommandText = "SELECT sp.transaction_id " +
                        "FROM student_payments sp, transactions t " +
                        "WHERE sp.student_id = @studentId " +
                        "AND sp.transaction_id = t.id " +
                        "ORDER BY t.t_time DESC";
                    cmd.Parameters.AddWithValue("studentId", student.Id);

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            // Getting transaction.
                            Transaction transaction = await Loader.GetTransactionByIdAsync(reader.GetString(0));

                            // Adding to list.
                            m_paymentHistory.Add(new PaymentHistoryViewModel(transaction));
                        }
                    }
                }
            }
        }

        void PrintTransaction(Transaction transaction)
        {
            // Generating report parameters.
            List<ReportParameter> reportParameters = new List<ReportParameter>();
            reportParameters.Add(new ReportParameter("TransactionId", transaction.Id));
            reportParameters.Add(new ReportParameter("PayerName", transaction.Payer));
            reportParameters.Add(new ReportParameter("Details", transaction.Details));
            reportParameters.Add(new ReportParameter("AmountString", transaction.Amount.ToString("C2")));
            reportParameters.Add(new ReportParameter("DateString", transaction.TTime.ToString("d MMMM yyyy")));

            // Showing print window.
            PrintWindow printWindow = new PrintWindow("Resources\\Templates\\ReceiptReportSmall.rdlc", reportParams: reportParameters);
            printWindow.Show();
        }

        private async void btnSearchStudentById_Click(object sender, RoutedEventArgs e)
        {
            m_selectedStudent = null;
            txtName.Clear();
            txtGradeName.Clear();
            txtProgrammeName.Clear();
            m_oncePayments.Clear();
            m_annualPayments.Clear();
            m_semesterPayments.Clear();
            m_monthPayments.Clear();

            m_mainwindow.PlayLoadingAnimation();
            btnSearchStudentById.IsEnabled = false;
            m_canGoBack = false;

            try
            {
                if (string.IsNullOrEmpty(txtId.Text))
                {
                    throw new Exception("Student id is empty");
                }
                
                m_selectedStudent = await Loader.GetStudentByStudentIdAsync(txtId.Text);

                txtName.Text = m_selectedStudent.Name;
                txtGradeName.Text = m_selectedStudent.Grade.Name;
                txtProgrammeName.Text = m_selectedStudent.Programme.Name;

                // Loading only once payments.
                await LoadOncePaymentsAsync(m_selectedStudent);

                // Loading history.
                await LoadPaymentHistoryAsync(m_selectedStudent);

                if (cmbYears_Annually.SelectedItem != null)
                {
                    await LoadAnnualPaymentsAsync(m_selectedStudent, Convert.ToInt32(cmbYears_Annually.SelectedItem as string));
                }

                if (cmbYears_Semester.SelectedItem != null && cmbSemester_Semester.SelectedItem != null)
                {
                    await LoadSemesterPaymentsAsync(m_selectedStudent, Convert.ToInt32(cmbYears_Semester.SelectedItem), cmbSemester_Semester.SelectedItem as string);
                }

                if (cmbYears_Monthly.SelectedItem != null && cmbMonths_Monthly.SelectedItem != null)
                {
                    await LoadMonthPaymentsAsync(m_selectedStudent, Convert.ToInt32(cmbYears_Monthly.SelectedItem), cmbMonths_Monthly.SelectedItem as string);
                }
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to find student.\nDetails: " + ex.Message);
            }
            finally
            {
                m_mainwindow.StopLoadingAnimation();
                btnSearchStudentById.IsEnabled = true;
                m_canGoBack = true;
            }
        }

        #region TAB_ONCE

        private void btnPay_Once_Click(object sender, RoutedEventArgs e)
        {
            PaymentViewModel selitem = lstOnce.SelectedItem as PaymentViewModel;

            if (selitem != null)
            {
                if (selitem.RemainingAmount == 0)
                {
                    m_mainwindow.ShowMessage("Error", "Selected payment is fully paid.");
                    return;
                }

                m_paymentBeingPaid = selitem;
                txtPayerName_gridPayment.Text = m_selectedStudent.Name;
                txtBillingTypeName_gridPayment.Text = selitem.BillingTypeName;
                txtTotalAmount_gridPayment.Text = selitem.TotalAmount.ToString("C0");
                txtPaidAmount_gridPayment.Text = selitem.PaidAmount.ToString("C0");
                txtRemainingAmount_gridPayment.Text = selitem.RemainingAmount.ToString("C0");

                cmbYear_gridPayment.IsEnabled = false;
                cmbSemester_gridPayment.IsEnabled = false;
                cmbMonth_gridPayment.IsEnabled = false;

                gridPayment.Visibility = Visibility.Visible;
                m_canGoBack = false;
            }
        }

        #endregion

        #region TAB_ANNUALLY

        private async void cmbYears_Annually_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbYears_Annually.SelectedItem != null && m_selectedStudent != null)
            {
                cmbYears_Annually.IsEnabled = false;

                try
                {
                    await LoadAnnualPaymentsAsync(m_selectedStudent, Convert.ToInt32(cmbYears_Annually.SelectedItem));
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to load payments of selected year.\nDetails: " + ex.Message);
                }
                finally
                {
                    cmbYears_Annually.IsEnabled = true;
                }
            }
        }

        private void btnPay_Annually_Click(object sender, RoutedEventArgs e)
        {
            PaymentViewModel selitem = lstAnnually.SelectedItem as PaymentViewModel;

            if (selitem != null)
            {
                if (selitem.RemainingAmount == 0)
                {
                    m_mainwindow.ShowMessage("Error", "Selected payment of the selected year is fully paid.");
                    return;
                }

                m_paymentBeingPaid = selitem;
                txtPayerName_gridPayment.Text = m_selectedStudent.Name;
                txtBillingTypeName_gridPayment.Text = selitem.BillingTypeName;
                txtTotalAmount_gridPayment.Text = selitem.TotalAmount.ToString("C0");
                txtPaidAmount_gridPayment.Text = selitem.PaidAmount.ToString("C0");
                txtRemainingAmount_gridPayment.Text = selitem.RemainingAmount.ToString("C0");
                cmbYear_gridPayment.SelectedItem = cmbYears_Annually.SelectedItem;

                cmbYear_gridPayment.IsEnabled = true;
                cmbSemester_gridPayment.IsEnabled = false;
                cmbMonth_gridPayment.IsEnabled = false;

                gridPayment.Visibility = Visibility.Visible;
                m_canGoBack = false;
            }
        }

        #endregion

        #region TAB_SEMESTER

        private async void cmbSemester_Semester_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (m_selectedStudent != null && cmbYears_Semester.SelectedItem != null && cmbSemester_Semester.SelectedItem != null)
            {
                cmbYears_Semester.IsEnabled = false;
                cmbSemester_Semester.IsEnabled = false;

                try
                {
                    await LoadSemesterPaymentsAsync(m_selectedStudent, Convert.ToInt32(cmbYears_Semester.SelectedItem), cmbSemester_Semester.SelectedItem as string);
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to load payments of selected year and semester.\nDetails: " + ex.Message);
                }
                finally
                {
                    cmbYears_Semester.IsEnabled = true;
                    cmbSemester_Semester.IsEnabled = true;
                }
            }
        }

        private void cmbYears_Semester_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cmbSemester_Semester_SelectionChanged(null, null);
        }

        private void btnPay_Semester_Click(object sender, RoutedEventArgs e)
        {
            PaymentViewModel selitem = lstSemester.SelectedItem as PaymentViewModel;

            if (selitem != null)
            {
                if (selitem.RemainingAmount == 0)
                {
                    m_mainwindow.ShowMessage("Error", "Selected payment of the selected year and semester is fully paid.");
                    return;
                }

                m_paymentBeingPaid = selitem;
                txtPayerName_gridPayment.Text = m_selectedStudent.Name;
                txtBillingTypeName_gridPayment.Text = selitem.BillingTypeName;
                txtTotalAmount_gridPayment.Text = selitem.TotalAmount.ToString("C0");
                txtPaidAmount_gridPayment.Text = selitem.PaidAmount.ToString("C0");
                txtRemainingAmount_gridPayment.Text = selitem.RemainingAmount.ToString("C0");
                cmbYear_gridPayment.SelectedItem = cmbYears_Semester.SelectedItem;
                cmbSemester_gridPayment.SelectedItem = cmbSemester_Semester.SelectedItem;

                cmbYear_gridPayment.IsEnabled = true;
                cmbSemester_gridPayment.IsEnabled = true;
                cmbMonth_gridPayment.IsEnabled = false;

                gridPayment.Visibility = Visibility.Visible;
                m_canGoBack = false;
            }
        }

        #endregion

        #region TAB_MONTHLY

        private async void cmbMonths_Monthly_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (m_selectedStudent != null && cmbYears_Monthly.SelectedItem != null && cmbMonths_Monthly.SelectedItem != null)
            {
                cmbYears_Monthly.IsEnabled = false;
                cmbMonths_Monthly.IsEnabled = false;

                try
                {
                    await LoadMonthPaymentsAsync(m_selectedStudent, Convert.ToInt32(cmbYears_Monthly.SelectedItem), cmbMonths_Monthly.SelectedItem as string);
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to load payments of selected year and month.\nDetails: " + ex.Message);
                }
                finally
                {
                    cmbYears_Monthly.IsEnabled = true;
                    cmbMonths_Monthly.IsEnabled = true;
                }
            }
        }

        private void cmbYears_Monthly_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cmbMonths_Monthly_SelectionChanged(null, null);
        }

        private void btnPay_Monthly_Click(object sender, RoutedEventArgs e)
        {
            PaymentViewModel selitem = lstMonthly.SelectedItem as PaymentViewModel;

            if (selitem != null)
            {
                if (selitem.RemainingAmount == 0)
                {
                    m_mainwindow.ShowMessage("Error", "Selected payment of the selected year and month is fully paid.");
                    return;
                }

                m_paymentBeingPaid = selitem;
                txtPayerName_gridPayment.Text = m_selectedStudent.Name;
                txtBillingTypeName_gridPayment.Text = selitem.BillingTypeName;
                txtTotalAmount_gridPayment.Text = selitem.TotalAmount.ToString("C0");
                txtPaidAmount_gridPayment.Text = selitem.PaidAmount.ToString("C0");
                txtRemainingAmount_gridPayment.Text = selitem.RemainingAmount.ToString("C0");
                cmbYear_gridPayment.SelectedItem = cmbYears_Monthly.SelectedItem;
                cmbMonth_gridPayment.SelectedItem = cmbMonths_Monthly.SelectedItem;

                cmbYear_gridPayment.IsEnabled = true;
                cmbSemester_gridPayment.IsEnabled = false;
                cmbMonth_gridPayment.IsEnabled = true;

                gridPayment.Visibility = Visibility.Visible;
                m_canGoBack = false;
            }
        }

        #endregion

        #region TAB_HISTORY

        private void btnPrint_History_Click(object sender, RoutedEventArgs e)
        {
            // Getting selected items.
            List<TransactionPrint> transactionPrints = new List<TransactionPrint>();
            int subtotal = 0;

            for (int i = 0; i < m_paymentHistory.Count; ++i)
            {
                if (m_paymentHistory[i].IsChecked)
                {
                    TransactionPrint print = new TransactionPrint(m_paymentHistory[i].Transaction);
                    transactionPrints.Add(print);

                    subtotal += m_paymentHistory[i].Transaction.Amount;
                }
            }

            if (transactionPrints.Count == 0)
            {
                m_mainwindow.ShowMessage("Error", "No transaction to be printed!");
                return;
            }

            // Earlier -> later ordering.
            ReportDataSource rds = new ReportDataSource("TransactionPrintDataSet", transactionPrints);

            List<ReportParameter> reportParameters = new List<ReportParameter>();
            reportParameters.Add(new ReportParameter("PayerName", m_selectedStudent.Name));
            reportParameters.Add(new ReportParameter("SubTotalString", subtotal.ToString("C")));
            reportParameters.Add(new ReportParameter("DateString", DateTime.Now.ToString("d MMMM yyyy")));

            PrintWindow p = new PrintWindow("Resources\\Templates\\ReceiptReport.rdlc", rds, reportParameters);
            p.Show();
        }

        #endregion

        private async void btnProceed_gridPayment_Click(object sender, RoutedEventArgs e)
        {
            btnProceed_gridPayment.IsEnabled = false;
            btnCancel_gridPayment.IsEnabled = false;

            try
            {
                int amount;

                if (!int.TryParse(txtAmount_gridPayment.Text, out amount))
                {
                    throw new Exception("Amount is empty or invalid");
                }

                if (amount > m_paymentBeingPaid.RemainingAmount)
                {
                    amount = m_paymentBeingPaid.RemainingAmount; // Dont exceed the remaining amount!
                }

                bool isFullyPaid = amount == m_paymentBeingPaid.RemainingAmount;
                string transactionId;

                using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
                {
                    await dbconn.OpenAsync();

                    // Initializing transaction.
                    using (MySqlTransaction transaction = await dbconn.BeginTransactionAsync())
                    {
                        DateTime now = DateTime.Now;

                        // Generating transaction id.
                        transactionId = await Loader.GenerateTransactionIdAsync(dbconn, transaction, now);

                        // Inserting to transactions table.
                        using (MySqlCommand cmd = new MySqlCommand())
                        {
                            cmd.Connection = dbconn;
                            cmd.Transaction = transaction;
                            cmd.CommandText = "INSERT INTO transactions(id, t_time, amount, payer, is_out, details) VALUES(@id, @now, @amount, @payer, 0, @details)";
                            cmd.Parameters.AddWithValue("id", transactionId);
                            cmd.Parameters.AddWithValue("now", now);
                            cmd.Parameters.AddWithValue("amount", amount);
                            cmd.Parameters.AddWithValue("payer", m_selectedStudent.Name);

                            string details;
                            int dayInterval = m_paymentBeingPaid.BillingLink.BillingType.DayInterval;
                            
                            switch (dayInterval)
                            {
                                case StaticData.PERIOD_ONCE:
                                    details = m_paymentBeingPaid.BillingTypeName;
                                    break;
                                case StaticData.PERIOD_ANNUALLY:
                                    details = string.Format("{0} - {1}", m_paymentBeingPaid.BillingTypeName, cmbYears_Annually.SelectedItem.ToString());
                                    break;
                                case StaticData.PERIOD_SEMESTER:
                                    details = string.Format("{0} - {1} {2}", m_paymentBeingPaid.BillingTypeName, cmbSemester_Semester.SelectedItem as string, cmbYears_Semester.SelectedItem.ToString());
                                    break;
                                case StaticData.PERIOD_MONTHLY:
                                    details = string.Format("{0} - {1} {2}", m_paymentBeingPaid.BillingTypeName, cmbMonths_Monthly.SelectedItem as string, cmbYears_Monthly.SelectedItem.ToString());
                                    break;
                                default:
                                    throw new Exception("Invalid parameters");
                            }

                            if (isFullyPaid)
                            {
                                details += " (LUNAS)";
                            }
                            else
                            {
                                details += " (BELUM LUNAS)";
                            }

                            cmd.Parameters.AddWithValue("details", details);
                            await cmd.ExecuteNonQueryAsync();
                        }

                        // Inserting to student_payments table.
                        using (MySqlCommand cmd = new MySqlCommand())
                        {
                            cmd.Connection = dbconn;
                            cmd.Transaction = transaction;

                            int dayInterval = m_paymentBeingPaid.BillingLink.BillingType.DayInterval;

                            if (dayInterval == StaticData.PERIOD_ONCE)
                            {
                                cmd.CommandText = "INSERT INTO student_payments(transaction_id, student_id, billing_type_id) VALUES(@tid, @sid, @btid)";
                            }
                            else if (dayInterval == StaticData.PERIOD_ANNUALLY)
                            {
                                cmd.CommandText = "INSERT INTO student_payments(transaction_id, student_id, billing_type_id, year) ";
                                cmd.CommandText += "VALUES(@tid, @sid, @btid, @year)";
                                cmd.Parameters.AddWithValue("year", Convert.ToInt32(cmbYear_gridPayment.SelectedItem));
                            }
                            else if (dayInterval == StaticData.PERIOD_SEMESTER)
                            {
                                cmd.CommandText = "INSERT INTO student_payments(transaction_id, student_id, billing_type_id, year, semester) ";
                                cmd.CommandText += "VALUES(@tid, @sid, @btid, @year, @semester)";
                                cmd.Parameters.AddWithValue("year", Convert.ToInt32(cmbYear_gridPayment.SelectedItem));
                                cmd.Parameters.AddWithValue("semester", cmbSemester_gridPayment.SelectedItem as string);
                            }
                            else if (dayInterval == StaticData.PERIOD_MONTHLY)
                            {
                                cmd.CommandText = "INSERT INTO student_payments(transaction_id, student_id, billing_type_id, year, month) ";
                                cmd.CommandText += "VALUES(@tid, @sid, @btid, @year, @month)";
                                cmd.Parameters.AddWithValue("year", Convert.ToInt32(cmbYear_gridPayment.SelectedItem));
                                cmd.Parameters.AddWithValue("month", cmbMonth_gridPayment.SelectedItem as string);
                            }

                            cmd.Parameters.AddWithValue("tid", transactionId);
                            cmd.Parameters.AddWithValue("sid", m_selectedStudent.Id);
                            cmd.Parameters.AddWithValue("btid", m_paymentBeingPaid.BillingLink.BillingType.Id);

                            await cmd.ExecuteNonQueryAsync();
                        }

                        transaction.Commit();
                    }
                }

                // Reloading list.
                switch (m_paymentBeingPaid.BillingLink.BillingType.DayInterval)
                {
                    case StaticData.PERIOD_ONCE:
                        await LoadOncePaymentsAsync(m_selectedStudent);
                        break;
                    case StaticData.PERIOD_ANNUALLY:
                        await LoadAnnualPaymentsAsync(m_selectedStudent, Convert.ToInt32(cmbYears_Annually.SelectedItem));
                        break;
                    case StaticData.PERIOD_SEMESTER:
                        await LoadSemesterPaymentsAsync(m_selectedStudent, Convert.ToInt32(cmbYears_Semester.SelectedItem), cmbSemester_Semester.SelectedItem as string);
                        break;
                    case StaticData.PERIOD_MONTHLY:
                        await LoadMonthPaymentsAsync(m_selectedStudent, Convert.ToInt32(cmbYears_Monthly.SelectedItem), cmbMonths_Monthly.SelectedItem as string);
                        break;
                    default:
                        throw new NotSupportedException();
                }

                await LoadPaymentHistoryAsync(m_selectedStudent);

                // Done.
                btnCancel_gridPayment_Click(null, null);
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to finish the transaction.\nDetails: " + ex.Message);
            }
            finally
            {
                btnCancel_gridPayment.IsEnabled = true;
                btnProceed_gridPayment.IsEnabled = true;
            }
        }

        private void btnCancel_gridPayment_Click(object sender, RoutedEventArgs e)
        {
            m_paymentBeingPaid = null;
            m_canGoBack = true;
            gridPayment.Visibility = Visibility.Hidden;

            txtPayerName_gridPayment.Clear();
            txtBillingTypeName_gridPayment.Clear();
            txtTotalAmount_gridPayment.Clear();
            txtPaidAmount_gridPayment.Clear();
            txtRemainingAmount_gridPayment.Clear();
            txtAmount_gridPayment.Clear();
        }

        private void btnPrint_Annually_Click(object sender, RoutedEventArgs e)
        {
            // Selecting items.
            List<UnpaidPayment> unpaidPayments = new List<UnpaidPayment>();

            for (int i = 0; i < m_annualPayments.Count; ++i)
            {
                if (m_annualPayments[i].RemainingAmount == 0 || !m_annualPayments[i].IsChecked)
                {
                    continue;
                }

                UnpaidPayment up = new UnpaidPayment();
                up.PaymentName = m_annualPayments[i].BillingTypeName;
                up.Amount = m_annualPayments[i].RemainingAmount;
                unpaidPayments.Add(up);
            }

            if (unpaidPayments.Count == 0)
            {
                return;
            }

            int subtotal = 0;

            for (int i = 0; i < unpaidPayments.Count; ++i)
            {
                subtotal += unpaidPayments[i].Amount;
            }

            // Printing.
            ReportDataSource rds = new ReportDataSource("UnpaidPaymentsDataSet", unpaidPayments);

            List<ReportParameter> reportParameters = new List<ReportParameter>();
            reportParameters.Add(new ReportParameter("StudentName", m_selectedStudent.Name));
            reportParameters.Add(new ReportParameter("StudentFullClassName", m_selectedStudent.Grade.Name + "-" + m_selectedStudent.Programme.Name));
            reportParameters.Add(new ReportParameter("SubTotalString", subtotal.ToString("C")));
            reportParameters.Add(new ReportParameter("DateString", DateTime.Now.ToString("d MMMM yyyy")));

            PrintWindow p = new PrintWindow("Resources\\Templates\\UnpaidPaymentReport.rdlc", rds, reportParameters);
            p.Show();
        }
    }
}
