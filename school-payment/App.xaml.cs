﻿using school_payment.Views;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace school_payment
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            // Setting up culture.
            CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("id-ID");
            CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("id-ID");

            new MainWindow().Show();
        }
    }
}
