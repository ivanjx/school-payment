﻿using MySql.Data.MySqlClient;
using school_payment.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace school_payment.Commons
{
    public static class Loader
    {
        public static async Task<string> GenerateTransactionIdAsync(
            MySqlConnection dbconn, 
            MySqlTransaction transaction,
            DateTime transactionTime)
        {
            using (MySqlCommand cmd = new MySqlCommand())
            {
                cmd.Connection = dbconn;
                cmd.Transaction = transaction;
                cmd.CommandText = "SELECT COUNT(id) FROM transactions WHERE t_time BETWEEN @today AND @tomorrow";
                cmd.Parameters.AddWithValue("today", transactionTime.Date);
                cmd.Parameters.AddWithValue("tomorrow", transactionTime.Date.AddDays(1));

                object resultObj = await cmd.ExecuteScalarAsync();
                int count = resultObj.GetType() == typeof(DBNull) ? 0 : Convert.ToInt32(resultObj);

                string id;

                while (true)
                {
                    id = string.Format("TR{0:yyyyMMdd}{1:D5}", transactionTime, ++count);
                    if (!await IsTransactionIdExists(dbconn, transaction, id))
                    {
                        return id;
                    }
                }
            }
        }

        public static async Task<bool> IsTransactionIdExists(
            MySqlConnection dbconn,
            MySqlTransaction transaction,
            string id)
        {
            using (MySqlCommand cmd = new MySqlCommand())
            {
                cmd.Connection = dbconn;
                cmd.Transaction = transaction;
                cmd.CommandText = "SELECT id FROM transactions WHERE id = @id";
                cmd.Parameters.AddWithValue("id", id);

                object resultObj = await cmd.ExecuteScalarAsync();
                return resultObj != null;
            }
        }

        public static string GetPeriodName(int dayInterval)
        {
            switch (dayInterval)
            {
                case StaticData.PERIOD_ONCE:
                    return StaticData.PERIOD_STRING_ONCE;
                case StaticData.PERIOD_ANNUALLY:
                    return StaticData.PERIOD_STRING_ANNUALLY;
                case StaticData.PERIOD_SEMESTER:
                    return StaticData.PERIOD_STRING_SEMESTER;
                case StaticData.PERIOD_MONTHLY:
                    return StaticData.PERIOD_STRING_MONTHLY;
                default:
                    throw new Exception("Invalid day interval");
            }
        }

        public static int GetDayIntervalPeriod(string periodName)
        {
            switch (periodName)
            {
                case StaticData.PERIOD_STRING_ONCE:
                    return StaticData.PERIOD_ONCE;
                case StaticData.PERIOD_STRING_ANNUALLY:
                    return StaticData.PERIOD_ANNUALLY;
                case StaticData.PERIOD_STRING_SEMESTER:
                    return StaticData.PERIOD_SEMESTER;
                case StaticData.PERIOD_STRING_MONTHLY:
                    return StaticData.PERIOD_MONTHLY;
                default:
                    throw new Exception("Invalid period name");
            }
        }


        public static async Task<Grade> GetGradeByIdAsync(int id)
        {
            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbconn;
                    cmd.CommandText = "SELECT * FROM grades WHERE id = @id";
                    cmd.Parameters.AddWithValue("id", id);

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        if (await reader.ReadAsync())
                        {
                            Grade g = new Grade();
                            g.Id = id;
                            g.Name = reader.GetString(1);

                            return g;
                        }

                        throw new Exception("Not found");
                    }
                }
            }
        }

        public static async Task<Programme> GetProgrammeByIdAsync(int id)
        {
            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbconn;
                    cmd.CommandText = "SELECT * FROM programmes WHERE id = @id";
                    cmd.Parameters.AddWithValue("id", id);

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        if (await reader.ReadAsync())
                        {
                            Programme p = new Programme();
                            p.Id = id;
                            p.Name = reader.GetString(1);

                            return p;
                        }

                        throw new Exception("Not found");
                    }
                }
            }
        }

        public static async Task<BillingType> GetBillingTypeByIdAsync(int id)
        {
            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbconn;
                    cmd.CommandText = "SELECT * FROM billing_types WHERE id = @id";
                    cmd.Parameters.AddWithValue("id", id);

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        if (await reader.ReadAsync())
                        {
                            BillingType bt = new BillingType();
                            bt.Id = id;
                            bt.Name = reader.GetString(1);
                            bt.DayInterval = reader.GetInt32(2);

                            return bt;
                        }

                        throw new Exception("Not found");
                    }
                }
            }
        }

        public static async Task<Student> GetStudentByIdAsync(int id)
        {
            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbconn;
                    cmd.CommandText = "SELECT * FROM students WHERE id = @id";
                    cmd.Parameters.AddWithValue("id", id);

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        if (await reader.ReadAsync())
                        {
                            Student s = new Student();
                            s.Id = reader.GetInt32(0);
                            s.StudentId = reader.GetString(1);
                            s.Name = reader.GetString(2);
                            s.Programme = await GetProgrammeByIdAsync(reader.GetInt32(3));
                            s.Grade = await GetGradeByIdAsync(reader.GetInt32(4));
                            s.Status = reader.GetString(5);

                            return s;
                        }

                        throw new Exception("Not found");
                    }
                }
            }
        }

        public static async Task<Student> GetStudentByStudentIdAsync(string id)
        {
            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbconn;
                    cmd.CommandText = "SELECT * FROM students WHERE student_id = @id";
                    cmd.Parameters.AddWithValue("id", id);

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        if (await reader.ReadAsync())
                        {
                            Student s = new Student();
                            s.Id = reader.GetInt32(0);
                            s.StudentId = id;
                            s.Name = reader.GetString(2);
                            s.Programme = await GetProgrammeByIdAsync(reader.GetInt32(3));
                            s.Grade = await GetGradeByIdAsync(reader.GetInt32(4));
                            s.Status = reader.GetString(5);

                            return s;
                        }

                        throw new Exception("Not found");
                    }
                }
            }
        }

        public static async Task<Transaction> GetTransactionByIdAsync(string id)
        {
            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbconn;
                    cmd.CommandText = "SELECT * FROM transactions WHERE id = @tid";
                    cmd.Parameters.AddWithValue("tid", id);

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        if (await reader.ReadAsync())
                        {
                            Transaction t = new Transaction();
                            t.Id = reader.GetString(0);
                            t.TTime = reader.GetDateTime(1);
                            t.Amount = reader.GetInt32(2);
                            t.Payer = reader.GetString(3);
                            t.IsOutTransaction = reader.GetBoolean(4);
                            t.Details = reader.GetString(5);

                            return t;
                        }

                        throw new Exception("Not found");
                    }
                }
            }
        }

        public static async Task<Grade> GetGradeByNameAsync(string gradeName)
        {
            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbconn;
                    cmd.CommandText = "SELECT id FROM grades WHERE name = @gradeName";
                    cmd.Parameters.AddWithValue("gradeName", gradeName);

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        if (await reader.ReadAsync())
                        {
                            Grade grade = new Grade();
                            grade.Id = reader.GetInt32(0);
                            grade.Name = gradeName;

                            return grade;
                        }

                        throw new Exception("Not found");
                    }
                }
            }
        }

        public static async Task<Programme> GetProgrammeByNameAsync(string programmeName)
        {
            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbconn;
                    cmd.CommandText = "SELECT id FROM programmes WHERE name = @programmeName";
                    cmd.Parameters.AddWithValue("programmeName", programmeName);

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        if (await reader.ReadAsync())
                        {
                            Programme p = new Programme();
                            p.Id = reader.GetInt32(0);
                            p.Name = programmeName;

                            return p;
                        }

                        throw new Exception("Not found");
                    }
                }
            }
        }

        public static async Task<TransactionCategory> GetTransactionCategoryByIdAsync(int id)
        {
            using (MySqlConnection dbconn = new MySqlConnection(StaticData.DbConnectionString))
            {
                await dbconn.OpenAsync();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbconn;
                    cmd.CommandText = "SELECT * FROM transaction_categories WHERE id = @id";
                    cmd.Parameters.AddWithValue("id", id);

                    using (MySqlDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        if (await reader.ReadAsync())
                        {
                            TransactionCategory tc = new TransactionCategory();
                            tc.Id = id;
                            tc.Name = reader.GetString(1);

                            return tc;
                        }

                        throw new Exception("Not found");
                    }
                }
            }
        }
    }
}
