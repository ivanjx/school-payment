﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace school_payment.Commons
{
    public class StaticData
    {
        public static string DbConnectionStringBase
        {
            get;
            set;
        }

        public static string DatabaseName
        {
            get;
            set;
        }

        public static string DbConnectionString
        {
            get
            {
                return DbConnectionStringBase + "database = " + DatabaseName + ";";
            }
        }

        public const int PERIOD_ONCE = 1000;
        public const int PERIOD_ANNUALLY = 365;
        public const int PERIOD_SEMESTER = 180;
        public const int PERIOD_MONTHLY = 30;

        public const string PERIOD_STRING_ONCE = "ONCE";
        public const string PERIOD_STRING_ANNUALLY = "ANNUALLY";
        public const string PERIOD_STRING_SEMESTER = "SEMESTER";
        public const string PERIOD_STRING_MONTHLY = "MONTHLY";

        public const string FIRST_SEMESTER = "JANUARI-JUNI";
        public const string SECOND_SEMESTER = "JULI-DESEMBER";

        public static readonly string[] MONTHS_NAME = {
            "JANUARI",
            "FEBRUARI",
            "MARET",
            "APRIL",
            "MEI",
            "JUNI",
            "JULI",
            "AGUSTUS",
            "SEPTEMBER",
            "OKTOBER",
            "NOVEMBER",
            "DESEMBER",
        };
    }
}
