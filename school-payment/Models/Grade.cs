﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace school_payment.Models
{
    public class Grade
    {
        public int Id { get; set; }

        public string Name { get; set; }


        public override bool Equals(object obj)
        {
            Grade comp = obj as Grade;

            if (comp != null)
            {
                return Id == comp.Id;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
