﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace school_payment.Models
{
    public class Student
    {
        public int Id { get; set; }

        public string StudentId { get; set; }

        public string Name { get; set; }

        public Programme Programme { get; set; }

        public Grade Grade { get; set; }

        public string Status { get; set; }


        public override bool Equals(object obj)
        {
            Student comp = obj as Student;

            if (comp != null)
            {
                return Id == comp.Id;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
