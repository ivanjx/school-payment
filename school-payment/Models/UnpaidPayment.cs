﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace school_payment.Models
{
    public class UnpaidPayment
    {
        public string PaymentName { get; set; }

        public int Amount { get; set; }
    }
}
