﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace school_payment.Models
{
    public class Transaction
    {
        public string Id { get; set; }

        public DateTime TTime { get; set; }

        public string Payer { get; set; }

        public int Amount { get; set; }

        public bool IsOutTransaction { get; set; }

        public string Details { get; set; }

        public TransactionCategory Category { get; set; }


        public override bool Equals(object obj)
        {
            Transaction comp = obj as Transaction;

            if (comp != null)
            {
                return Id == comp.Id;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
