﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace school_payment.Models
{
    public class SavingSummary
    {
        public string StudentId
        {
            get;
            set;
        }

        public string StudentName
        {
            get;
            set;
        }

        public int TotalAmount
        {
            get;
            set;
        }

        public DateTime LastTransactionTime
        {
            get;
            set;
        }
    }
}
