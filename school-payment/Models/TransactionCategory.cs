﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace school_payment.Models
{
    public class TransactionCategory
    {
        public int Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }


        public override bool Equals(object obj)
        {
            TransactionCategory comp = obj as TransactionCategory;

            if (comp != null)
            {
                return Id == comp.Id;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
