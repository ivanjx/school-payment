﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace school_payment.Models
{
    public class BillingLink
    {
        public Programme Programme { get; set; }

        public BillingType BillingType { get; set; }

        public int Amount { get; set; }


        public override bool Equals(object obj)
        {
            BillingLink comp = obj as BillingLink;

            if (comp != null)
            {
                return Programme.Equals(comp.Programme) && BillingType.Equals(comp.BillingType);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
