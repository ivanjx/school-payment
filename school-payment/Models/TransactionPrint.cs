﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace school_payment.Models
{
    public class TransactionPrint
    {
        public Transaction Transaction
        {
            get;
            private set;
        }

        string m_amountStr, m_dateStr;

        public string Id
        {
            get
            {
                return Transaction.Id;
            }
        }

        public string AmountString
        {
            get
            {
                return m_amountStr;
            }
        }

        public string DateString
        {
            get
            {
                return m_dateStr;
            }
        }

        public string TypeString
        {
            get
            {
                return Transaction.IsOutTransaction ? "Keluar" : "Masuk";
            }
        }

        public string Details
        {
            get
            {
                return Transaction.Details;
            }
        }


        public TransactionPrint(Transaction transaction)
        {
            Transaction = transaction;
            m_amountStr = Transaction.Amount.ToString("C");
            m_dateStr = Transaction.TTime.ToString("d MMMM yyyy");
        }

        public override bool Equals(object obj)
        {
            TransactionPrint comp = obj as TransactionPrint;

            if (comp != null)
            {
                return Transaction.Equals(comp.Transaction);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
