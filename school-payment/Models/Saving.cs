﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace school_payment.Models
{
    public class Saving
    {
        public Student Student { get; set; }

        public DateTime TTime { get; set; }

        public bool IsOut { get; set; }

        public int Amount { get; set; }

        public string Type
        {
            get
            {
                return IsOut ? "OUT" : "IN";
            }
        }


        public override bool Equals(object obj)
        {
            Saving comp = obj as Saving;

            if (comp != null)
            {
                return Student.Equals(comp.Student) && TTime == comp.TTime;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
